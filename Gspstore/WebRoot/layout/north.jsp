<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" charset="utf-8">
	function logout(b) {
		$.post('userAction!logout.action', function() {
			if (b) {
				if (sy.isLessThanIe8()) {
					//loginAndRegDialog.dialog('open');
					window.location.href="login.jsp";
				} else {
					location.replace(sy.bp());
				}
			} else {
				//loginAndRegDialog.dialog('open');
				window.location.href="login.jsp";
			}
		});
	}

	var userInfoWindow;
	function showUserInfo() {
		userInfoWindow = $('<div/>').window({
			modal : true,
			title : '当前用户信息',
			width : 350,
			height : 300,
			collapsible : false,
			minimizable : false,
			maximizable : false,
			href : 'userAction!showUserInfo.action',
			onClose : function() {
				$(this).window('destroy');
			}
		});
	}
	
	var iframeDialog;
	var showCdescDialog;
	$(function() {
		iframeDialog = $('#iframeDialog').show().dialog({
			title : 'POP弹出页面',
			modal : true,
			closed : true,
			maximizable : false,
			onClose : function(){
			    document.getElementById("iframe").src = "about:blank";
			    location.reload();
			}
		});
		showCdescDialog = $('#showCdescDialog').show().dialog({
			title : '内容',
			modal : true,
			closed : true,
			maximizable : true,
		});
		$('#informMenu').menubutton({
	        iconCls: 'icon-tip',
	        menu: '#layout_north_informMenu',
	        text: "0条通知"
	    });
		//添加分隔线
		$('#layout_north_informMenu').menu('appendItem', {
			id:"informSeparator",
			separator: true
		});
		$('#layout_north_informMenu').menu('appendItem', {
			text: '查看全部通知',
			id:"informAll",
			onclick: function(){showInform();}
		});
		$('#noticeMenu').menubutton({
	        iconCls: 'icon-tip',
	        menu: '#layout_north_noticeMenu',
	        text: "0条公告/公文"
	    });
		//添加分隔线
		$('#layout_north_noticeMenu').menu('appendItem', {
			id:"noticeSeparator",
			separator: true
		});
		$('#layout_north_noticeMenu').menu('appendItem', {
			text: '查看全部公告/公文',
			id:"noticeAll",
			onclick: function(){showNotice();}
		});
		initWebSocket();
	});
	
	
	function initWebSocket(){
		var socket;
		if(!window.WebSocket){
			console.info("尝试使用MozWebSocket");
			window.WebSocket = window.MozWebSocket;
		}
		var wsUri = "ws:localhost:8080/Gspstore/noticeServer?userId=${sessionScope.sessionInfo.userId }";
		socket = new WebSocket(wsUri);
		socket.onmessage = function(event){
			console.info("receive a menu"+event.data);
			addMenuItem(event.data);
		};
		socket.onopen = function(event){
			//alert("websocket open success")
			console.info("websocket open success"+event);
		};
		socket.onerror = function(event){
			$.messager.alert('提示','可能浏览器不支持WebSocket,请更换浏览器后重试，否则通知功能将无法使用！','error');
			console.info(event);
			socket.close();
		};
	}
	
	function addMenuItem(data){
		//根据websocket返回的结果 修改菜单栏
		data = eval("(" + data + ")");//字符串转成json
			if(data.isAnnouncement==0){
				// 移除分隔行和最后一行
				$('#layout_north_informMenu').menu('removeItem', $('#informSeparator')[0]);
				$('#layout_north_informMenu').menu('removeItem', $('#informAll')[0]);
				//添加通知
				if(new Date(data.expiryTime.replace(/-/g,"/"))-new Date()<86400000){
					//24小时后失效
					$('#layout_north_informMenu').menu('appendItem', {
						text: "<font color='red'><strong>"+data.title+"</strong></font>",
						iconCls: "icon-cancel",
						id:data.noticeId,
						onclick: function(){
							showCdesc("inform",this);
						}
					});
				}else{
					$('#layout_north_informMenu').menu('appendItem', {
						text: "<strong>"+data.title+"</strong>",
						id:data.noticeId,
						onclick: function(){
							showCdesc("inform",this);
						}
					});
				}
				//添加分隔线
				$('#layout_north_informMenu').menu('appendItem', {
					id:"informSeparator",
					separator: true
				});
				$('#layout_north_informMenu').menu('appendItem', {
					text: '查看全部通知',
					id:"informAll",
					onclick: function(){showInform();}
				});
				var text = $('#informMenu')[0].text;
				text = (Number(text.split("条")[0])+1)+"条通知";
				$('#informMenu').menubutton({
			        iconCls: 'icon-tip',
			        menu: '#layout_north_informMenu',
			        text: text
			    });
			}else{
				// 移除分隔行和最后一行
				$('#layout_north_noticeMenu').menu('removeItem', $('#noticeSeparator')[0]);
				$('#layout_north_noticeMenu').menu('removeItem', $('#noticeAll')[0]);
				//添加公告/公文
				if(new Date(data.expiryTime.replace(/-/g,"/"))-new Date()<86400000){
					//24小时后失效
					$('#layout_north_noticeMenu').menu('appendItem', {
						text: "<font color='red'><strong>"+data.title+"</strong></font>",
						iconCls: "icon-cancel",
						id:data.noticeId,
						onclick: function(){
							showCdesc("notice",this);
						}
					});
				}else{
					$('#layout_north_noticeMenu').menu('appendItem', {
						text: "<strong>"+data.title+"</strong>",
						id:data.noticeId,
						onclick: function(){
							showCdesc("notice",this);
						}
					});
				}
				//添加分隔线
				$('#layout_north_noticeMenu').menu('appendItem', {
					id:"noticeSeparator",
					separator: true
				});
				$('#layout_north_noticeMenu').menu('appendItem', {
					text: '查看全部公告/公文',
					id:"noticeAll",
					onclick: function(){showNotice();}
				});
				var text = $('#noticeMenu')[0].text;
				text = (Number(text.split("条")[0])+1)+"条公告/公文";
				$('#noticeMenu').menubutton({
			        iconCls: 'icon-tip',
			        menu: '#layout_north_noticeMenu',
			        text: text
			    });
			}
	}
	
	function showInform(){
		$('#iframeDialog').dialog('setTitle', '我的通知'); 
		iframeDialog.dialog('open');
		$("#loading").css("display","block");
		document.getElementById("iframe").src = "${pageContext.request.contextPath}/noticeAction!goMyInform.action";
	}
	function showNotice(){
		$('#iframeDialog').dialog('setTitle', '我的公告/公文'); 
		iframeDialog.dialog('open');
		$("#loading").css("display","block");
		document.getElementById("iframe").src = "${pageContext.request.contextPath}/noticeAction!goMyNotice.action";
	}
	function showCdesc(flag,menuItem){
		$.messager.progress({
			text : '数据加载中....',
			interval : 100
		});
		$.ajax({
			url : 'noticeAction!readNotice.action',
			data : {
				noticeId : menuItem.id
			},
			dataType : 'json',
			cache : false,
			success : function(response) {
				$.messager.progress('close');
				if (response && response.context) {
					showCdescDialog.find('div[name=cdesc]').html(response.context);
					showCdescDialog.dialog('open');
				} else {
					$.messager.alert('提示', '没有内容！', 'error');
				}
				// 移除菜单项
				if(flag=="inform"){
					$('#layout_north_informMenu').menu('removeItem', menuItem);
					var text = $('#informMenu')[0].text;
					text = (Number(text.split("条")[0])-1)+"条通知";
					$('#informMenu').menubutton({
				        iconCls: 'icon-tip',
				        menu: '#layout_north_informMenu',
				        text: text
				    });
				}else if(flag=="notice"){
					$('#layout_north_noticeMenu').menu('removeItem', menuItem);
					var text = $('#noticeMenu')[0].text;
					text = (Number(text.split("条")[0])-1)+"条公告/公文";
					$('#noticeMenu').menubutton({
				        iconCls: 'icon-tip',
				        menu: '#layout_north_noticeMenu',
				        text: text
				    });
				}
			}
		});
	}
</script>
<div style="position: absolute; right: 0px; bottom: 0px; ">欢迎&nbsp;<B>${sessionScope.sessionInfo.realName }</B>&nbsp;光临！
	<!--<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_pfMenu" iconCls="icon-ok">更换皮肤</a> -->
	<a id="informMenu" href="javascript:void(0);" class="easyui-menubutton"></a>
	<a id="noticeMenu" href="javascript:void(0);" class="easyui-menubutton"></a>
	<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_kzmbMenu" iconCls="icon-help">控制面板</a>
	<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_zxMenu" iconCls="icon-back">退出</a>
</div>
<div id="layout_north_informMenu" style="width: 160px; display: none;">
</div>
<div id="layout_north_noticeMenu" style="width: 180px; display: none;">
</div>
<div id="iframeDialog" style="display: none;overflow: auto;width: 620px;height: 430px;">
	<div id="loading"  style="position: absolute;z-index: 10000;
         top:30;left:0;width:100%;height:95%;background: #DDDDDB  url('<%=request.getContextPath()%>/css/images/loading.gif') no-repeat center ;text-align:center;">
    </div>
	<iframe name="iframe" id="iframe" src=""  scrolling="no" frameborder="0" style="width:100%;height:100%;">
    </iframe>
</div>
<div id="showCdescDialog" style="display: none;overflow: auto;width: 500px;height: 400px;">
	<div name="cdesc"></div>
</div>


<div id="layout_north_pfMenu" style="width: 120px; display: none;">
	<div onclick="sy.changeTheme('default');">default</div>
	<div onclick="sy.changeTheme('gray');">gray</div>
	<div onclick="sy.changeTheme('cupertino');">cupertino</div>
	<div onclick="sy.changeTheme('dark-hive');">dark-hive</div>
	<div onclick="sy.changeTheme('pepper-grinder');">pepper-grinder</div>
	<div onclick="sy.changeTheme('sunny');">sunny</div>
</div>
<div id="layout_north_kzmbMenu" style="width: 100px; display: none;">
	<div onclick="showUserInfo();">个人信息</div>
	<div class="menu-sep"></div>
	<div>
		<span>更换主题</span>
		<div style="width: 120px;">
			<div onclick="sy.changeTheme('default');">default</div>
			<div onclick="sy.changeTheme('gray');">gray</div>
			<div onclick="sy.changeTheme('cupertino');">cupertino</div>
			<div onclick="sy.changeTheme('dark-hive');">dark-hive</div>
			<div onclick="sy.changeTheme('pepper-grinder');">pepper-grinder</div>
			<div onclick="sy.changeTheme('sunny');">sunny</div>
		</div>
	</div>
</div>
<div id="layout_north_zxMenu" style="width: 100px; display: none;">
	<div onclick="logout(true);">锁定窗口</div>
	<div class="menu-sep"></div>
	<div onclick="logout();">切换用户</div>
	<div onclick="logout(true);">退出系统</div>
</div>
