<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" charset="utf-8">
	function logout(b) {
		$.post('userAction!logout.action', function() {
			if (b) {
				if (sy.isLessThanIe8()) {
					//loginAndRegDialog.dialog('open');
					window.location.href="login.jsp";
				} else {
					location.replace(sy.bp());
				}
			} else {
				//loginAndRegDialog.dialog('open');
				window.location.href="login.jsp";
			}
		});
	}

	var userInfoWindow;
	function showUserInfo() {
		userInfoWindow = $('<div/>').window({
			modal : true,
			title : '当前用户信息',
			width : 350,
			height : 300,
			collapsible : false,
			minimizable : false,
			maximizable : false,
			href : 'userAction!showUserInfo.action',
			onClose : function() {
				$(this).window('destroy');
			}
		});
	}
	
	var iframeDialog;
	var showCdescDialog;
	$(function() {
		iframeDialog = $('#iframeDialog').show().dialog({
			title : 'POP弹出页面',
			modal : true,
			closed : true,
			maximizable : false,
			onClose : function(){
			    document.getElementById("iframe").src = "about:blank";
			}
		});
		showCdescDialog = $('#showCdescDialog').show().dialog({
			title : '通知内容',
			modal : true,
			closed : true,
			maximizable : true
		});
		// 定时刷新 时间间隔为10分钟
		setInterval("getMyInform()",600000); 
		setInterval("getMyNotice()",600000); 
		getMyInform();
		getMyNotice();
	});
	
	function getMyInform(){
		//取得我的通知信息
		$.ajax({
			url : 'noticeAction!datagridUnGroup.action?queryType=inform&queryUnRead=true',
			dataType : 'json',
			cache : false,
			success : function(response) {
				//if(response.total>0){
				//}
				$('#informMenu').menubutton({
			        iconCls: 'icon-tip',
			        menu: '#layout_north_informMenu',
			        text: response.total+"条通知"
			    });
				$('#layout_north_informMenu').empty();  
				var temp;
				for(var i = 0;i<response.rows.length;i++){
					temp = response.rows[i];
					if(new Date(temp.expiryTime.replace(/-/g,"/"))-new Date()<86400000){
						//24小时后失效
						$('#layout_north_informMenu').menu('appendItem', {
							text: "<font color='red'><strong>"+temp.title+"</strong></font>",
							iconCls: "icon-cancel",
							onclick: function(){
								showCdesc(temp.noticeId);
							}
						});
					}else{
						$('#layout_north_informMenu').menu('appendItem', {
							text: "<strong>"+temp.title+"</strong>",
							onclick: function(){
								showCdesc(temp.noticeId);
							}
						});
					}
				}
				//添加分隔线
				$('#layout_north_informMenu').menu('appendItem', {
					separator: true
				});
				$('#layout_north_informMenu').menu('appendItem', {
					text: '查看全部通知',
					onclick: function(){showInform();}
				});
			}
		});
	}
	
	function getMyNotice(){
		//取得我的通知信息
		$.ajax({
			url : 'noticeAction!datagridUnGroup.action?queryType=notice&queryUnRead=true',
			dataType : 'json',
			cache : false,
			success : function(response) {
				//if(response.total>0){
				//}
				$('#noticeMenu').menubutton({
			        iconCls: 'icon-tip',
			        menu: '#layout_north_noticeMenu',
			        text: response.total+"条公告/公文"
			    });
				$('#layout_north_noticeMenu').empty();  
				var temp;
				for(var i = 0;i<response.rows.length;i++){
					temp = response.rows[i];
					if(new Date(temp.expiryTime.replace(/-/g,"/"))-new Date()<86400000){
						//24小时后失效
						$('#layout_north_noticeMenu').menu('appendItem', {
							text: "<font color='red'><strong>"+temp.title+"</strong></font>",
							iconCls: "icon-cancel",
							onclick: function(){
								showCdesc(temp.noticeId);
							}
						});
					}else{
						$('#layout_north_noticeMenu').menu('appendItem', {
							text: "<strong>"+temp.title+"</strong>",
							onclick: function(){
								showCdesc(temp.noticeId);
							}
						});
					}
				}
				//添加分隔线
				$('#layout_north_noticeMenu').menu('appendItem', {
					separator: true
				});
				$('#layout_north_noticeMenu').menu('appendItem', {
					text: '查看全部公告/公文',
					onclick: function(){showNotice();}
				});
			}
		});
	}
	
	function showInform(){
		$('#iframeDialog').dialog('setTitle', '我的通知'); 
		iframeDialog.dialog('open');
		$("#loading").css("display","block");
		document.getElementById("iframe").src = "${pageContext.request.contextPath}/noticeAction!goMyInform.action";
	}
	function showNotice(){
		$('#iframeDialog').dialog('setTitle', '我的公告/公文'); 
		iframeDialog.dialog('open');
		$("#loading").css("display","block");
		document.getElementById("iframe").src = "${pageContext.request.contextPath}/noticeAction!goMyNotice.action";
	}
	function showCdesc(noticeId){
		$.messager.progress({
			text : '数据加载中....',
			interval : 100
		});
		$.ajax({
			url : 'noticeAction!readNotice.action',
			data : {
				noticeId : noticeId
			},
			dataType : 'json',
			cache : false,
			success : function(response) {
				$.messager.progress('close');
				if (response && response.context) {
					showCdescDialog.find('div[name=cdesc]').html(response.context);
					showCdescDialog.dialog('open');
				} else {
					$.messager.alert('提示', '没有内容！', 'error');
				}
				//TODO刷新一次，否则第二次查看的时候看到的是第一次的内容
				getMyInform();
				getMyNotice();
			}
		});
	}
</script>
<div style="position: absolute; right: 0px; bottom: 0px; ">欢迎&nbsp;<B>${sessionScope.sessionInfo.realName }</B>&nbsp;光临！
	<!--<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_pfMenu" iconCls="icon-ok">更换皮肤</a> -->
	<a id="informMenu" href="javascript:void(0);" class="easyui-menubutton">通知</a>
	<a id="noticeMenu" href="javascript:void(0);" class="easyui-menubutton">公告/公文</a>
	<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_kzmbMenu" iconCls="icon-help">控制面板</a>
	<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_zxMenu" iconCls="icon-back">退出</a>
</div>
<div id="layout_north_informMenu" style="width: 160px; display: none;">
</div>
<div id="layout_north_noticeMenu" style="width: 180px; display: none;">
</div>
<div id="iframeDialog" style="display: none;overflow: auto;width: 620px;height: 430px;">
	<div id="loading"  style="position: absolute;z-index: 10000;
         top:30;left:0;width:100%;height:95%;background: #DDDDDB  url('<%=request.getContextPath()%>/css/images/loading.gif') no-repeat center ;text-align:center;">
    </div>
	<iframe name="iframe" id="iframe" src=""  scrolling="no" frameborder="0" style="width:100%;height:100%;">
    </iframe>
</div>
<div id="showCdescDialog" style="display: none;overflow: auto;width: 500px;height: 400px;">
	<div name="cdesc"></div>
</div>


<div id="layout_north_pfMenu" style="width: 120px; display: none;">
	<div onclick="sy.changeTheme('default');">default</div>
	<div onclick="sy.changeTheme('gray');">gray</div>
	<div onclick="sy.changeTheme('cupertino');">cupertino</div>
	<div onclick="sy.changeTheme('dark-hive');">dark-hive</div>
	<div onclick="sy.changeTheme('pepper-grinder');">pepper-grinder</div>
	<div onclick="sy.changeTheme('sunny');">sunny</div>
</div>
<div id="layout_north_kzmbMenu" style="width: 100px; display: none;">
	<div onclick="showUserInfo();">个人信息</div>
	<div class="menu-sep"></div>
	<div>
		<span>更换主题</span>
		<div style="width: 120px;">
			<div onclick="sy.changeTheme('default');">default</div>
			<div onclick="sy.changeTheme('gray');">gray</div>
			<div onclick="sy.changeTheme('cupertino');">cupertino</div>
			<div onclick="sy.changeTheme('dark-hive');">dark-hive</div>
			<div onclick="sy.changeTheme('pepper-grinder');">pepper-grinder</div>
			<div onclick="sy.changeTheme('sunny');">sunny</div>
		</div>
	</div>
</div>
<div id="layout_north_zxMenu" style="width: 100px; display: none;">
	<div onclick="logout(true);">锁定窗口</div>
	<div class="menu-sep"></div>
	<div onclick="logout();">切换用户</div>
	<div onclick="logout(true);">退出系统</div>
</div>
