<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<style>
.huibd {
	border: 1px solid #000000;
}

td {
	white-space: nowrap;
	font-size: 12px;
}
</style>
<link rel="stylesheet"
	href="<%=basePath%>/main/include/css/button-usr.css" type="text/css"></link>
<script type="text/javascript"
	src="<%=basePath%>/main/include/js/form_valid.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/jslib/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$("#doReturn").click(function(){
	 window.parent.window.closePayoutDialog();
	});
$("#doReturn").attr("disabled",false);
$(window.parent.document.getElementById("loading")).fadeOut("slow",function(){
     $(this).css("display","none");
});
});
</script>
<style type="text/css">
td{
	text-align: center;
}
</style>
</head>
<body>
	
	<div style="width: 850px; " align="center">
		<table class="tableForm">
			<tr>
				<td>流程ID</td>
				<td>${historyProcPage.procid }</td>
				<td>流程名称</td>
				<td colspan="2">${historyProcPage.procname }</td>
				<td>发起人</td>
				<td colspan="2">${historyProcPage.startUser }</td>
			</tr>
			<tr>
				<td>流程开始时间</td>
				<td colspan="3"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${historyProcPage.startDate }"/></td>
				<td>流程结束时间</td>
				<td colspan="3"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${historyProcPage.endDate }"/></td>
			</tr>
			<tr>
				<td>流程图</td>
				<td colspan="7"><img alt="【${historyProcPage.procid }】流程图" width="550px" height="300px"
					src="<%=basePath %>${historyProcPage.diagramPath }"></td>
			</tr>
			<s:if test="historyProcPage.historyTasks.size > 0">
				<tr>
					<td>历史任务</td>
				</tr>
				<tr>
					<td>任务ID</td>
					<td>任务名称</td>
					<td>任务描述</td>
					<td>任务执行者</td>
					<td>任务签收时间</td>
					<td>任务开始时间</td>
					<td>任务结束时间</td>
					<td>任务持续时间</td>
				</tr>
				<s:iterator value="historyProcPage.historyTasks" var="task"
					status="stuts">>
					<tr>
						<td>${task.taskid }</td>
						<td>${task.taskname }</td>
						<td>${task.taskDescription }</td>
						<td>${task.taskAssignee }</td>
						<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${task.taskAssignDate }"/></td>
						<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${task.taskStartDate }"/></td>
						<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${task.taskEndDate }"/></td>
						<td>${task.taskDurationInMillis }</td>
					</tr>
				</s:iterator>
			</s:if>
			<tr>
				<td height="50" colspan="8" align="center"><input value=" 关 闭 "
					type="button" id="doReturn" /></td>
			</tr>
		</table>
	</div>
</body>
</html>