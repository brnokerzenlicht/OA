<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<script type="text/javascript" charset="utf-8">
    var searchForm;
	var datagrid;
	var iframeDialog;
	$(function() {
	    //查询列表	
	    searchForm = $('#searchForm').form();
		datagrid = $('#datagrid').datagrid({
			url : 'historyProcAction!datagrid.action?procType=involved',
			title : '历史流程列表',
			iconCls : 'icon-save',
			pagination : true,
			pagePosition : 'bottom',
			rownumbers : true,
			pageSize : 10,
			pageList : [ 10, 20, 30, 40 ],
			fit : true,
			fitColumns : false,
			singleSelect:true,
			nowrap : true,
			border : false,
			idField : 'procid',
			sortName : 'startDate',
			sortOrder : 'desc',
			columns : [ [ 
			{field:'ck',checkbox:true,
						formatter:function(value,row,index){
							return row.procid;
						}
					},
				{field:'procid',title:'流程ID',align:'center',sortable:false,width:80,
					formatter:function(value,row,index){
						return row.procid;
					}
				},	
				{field:'procname',title:'流程名称',align:'center',sortable:false,width:80,
					formatter:function(value,row,index){
						return row.procname;
					}
				},	
				{field:'startUser',title:'发起人',align:'center',sortable:false,width:120,
					formatter:function(value,row,index){
						return row.startUser;
					}
				},	
			   {field:'startDate',title:'开始时间',align:'center',sortable:false,width:130,
					formatter:function(value,row,index){
						return row.startDate;
					}
				},				
			   {field:'endDate',title:'结束时间',align:'center',sortable:false,width:130,
					formatter:function(value,row,index){
						return row.endDate;
					}
				}				
			 ] ],
			toolbar : [ {
				text : '历史流程详情',
				iconCls : 'icon-newd',
				handler : function() {
					detail();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'icon-relo',
				handler : function() {
					datagrid.datagrid('reload');
				}
			}, '-', {
				text : '取消选中',
				iconCls : 'icon-undo',
				handler : function() {
					datagrid.datagrid('unselectAll');
				}
			}, '-' ],
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
		});

		iframeDialog = $('#iframeDialog').show().dialog({
			title : 'POP弹出页面',
			modal : true,
			closed : true,
			maximizable : false,
			onClose : function(){
			    document.getElementById("iframe").src = "about:blank";
			}
		});

	});

	function _search() {
		datagrid.datagrid('load', sy.serializeObject(searchForm));
	}
	function cleanSearch() {
		datagrid.datagrid('load', {});
		searchForm.find('input').val('');
	}
	function detail() {
		var rows = datagrid.datagrid('getSelections');
		if (rows.length == 1) {
			$('#iframeDialog').dialog('setTitle', '【'+rows[0].procid+'】流程详情'); 
			iframeDialog.dialog('open');
			$("#loading").css("display","block");
			document.getElementById("iframe").src = "historyProcAction!detail.action?procid="+rows[0].procid;
		} else {
			$.messager.alert('提示', '请选择一项要查看的记录！', 'error');
		}
	} 
</script>
</head>
<body class="easyui-layout">
	<div region="north" border="false" title="过滤条件" collapsed="true"  style="height: 60px;overflow: hidden;display: none;" align="left">
		<form id="searchForm">
			<table class="tableForm datagrid-toolbar" style="width: 100%;height: 100%;">
				<tr>
					<td >流程ID</td>
					<td><input id="procid" name="procid" style="width:117px;" />
		 		    </td>
					<td >流程名称</td>
					<td><input id="procname" name="procname" style="width:117px;" />
		 		    </td>
					<td >流程开始时间</td>
					<td>
						<input id="beginStartDate" name="beginStartDate" style="width:117px;" class="easyui-datebox"/>
					   	<input id="endStartDate" name="endStartDate" style="width:117px;" class="easyui-datebox"/>
		 		    </td>
					<td >流程结束时间</td>
					<td>
						<input id="beginStartDate" name="beginStartDate" style="width:117px;" class="easyui-datebox"/>
					   	<input id="endStartDate" name="endStartDate" style="width:117px;" class="easyui-datebox"/>
		 		    </td>
			    <td><a href="javascript:void(0);" class="easyui-linkbutton" onclick="_search();">过滤</a><a href="javascript:void(0);" class="easyui-linkbutton" onclick="cleanSearch();">取消</a></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div region="center" border="false">
		<table id="datagrid"></table>
	</div>

	<div id="menu" class="easyui-menu" style="width:120px;display: none;">
		<div onclick="detail();" iconCls="icon-newd">历史流程详情</div>
	</div>
	
	<div id="iframeDialog" style="display: none;overflow: hidden;width: 900px;height: 600px;">
	<div id="loading"  style="position: absolute;z-index: 10000;
          top:30;left:0;width:100%;height:95%;background: #DDDDDB  url('<%=request.getContextPath()%>/css/images/loading.gif') no-repeat center ;text-align:center;">
        </div>
	<iframe name="iframe" id="iframe" src=""  scrolling="auto" frameborder="0" style="width:100%;height:100%;">
    </iframe>
</body>
</html>