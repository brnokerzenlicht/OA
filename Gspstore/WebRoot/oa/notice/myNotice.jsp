<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<script type="text/javascript" charset="utf-8">
	var datagrid;
	var showCdescDialog;
	$(document).ready(function() {
		$(window.parent.document.getElementById("loading")).fadeOut("slow",function(){
		     $(this).css("display","none");
		});
	    //查询列表	
	    searchForm = $('#searchForm').form();
		datagrid = $('#datagrid').datagrid({
			url : 'noticeAction!datagridUnGroup.action?queryType=notice&isQueryUnRead=false',
			title : '公告/公文列表',
			pagination : true,
			pagePosition : 'bottom',
			rownumbers : true,
			pageSize : 10,
			pageList : [ 10, 20, 30, 40 ],
			fit : true,
			fitColumns : true,
			nowrap : true,
			border : false,
			idField : 'noticeId',
			sortName : 'releaseTime',
			sortOrder : 'desc',
			columns : [ [ 
				{field:'isDealed',title:'状态',align:'center',sortable:true,
					formatter:function(value,row,index){
						if(row.isDealed==0){
							return "<font color='red'><strong>未读</strong></font>";
						}else{
							return "已读";
						}
					}
				},
			   {field:'title',title:'标题',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.title;
					}
				},				
			   {field:'releaseTime',title:'发布时间',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.releaseTime;
					}
				},					
			   {field:'depart',title:'发布部门',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.depart;
					}
				},				
			   {field:'remark',title:'备注',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.remark;
					}
				},
				{field:'context',title:'操作',align:'center',sortable:true,
					formatter:function(value,row,index){
						return '<span class="icon-search" style="display:inline-block;vertical-align:middle;width:16px;height:16px;"></span><a href="javascript:void(0);" onclick="showCdesc(' + index + ');">阅读</a>';
					}
				}
			 ] ]
		});

		showCdescDialog = $('#showCdescDialog').show().dialog({
			title : '公告/公文内容',
			modal : true,
			closed : true,
			maximizable : true
		});
	});

	function showCdesc(index) {
		if(showCdescDialog==undefined){
			//在iframe中显示该页面时，使用父窗口的dialog弹出
			showCdescDialog = parent.showCdescDialog;
		}
		var rows = datagrid.datagrid('getRows');
		var row = rows[index];
		$.messager.progress({
			text : '数据加载中....',
			interval : 100
		});
		$.ajax({
			url : 'noticeAction!readNotice.action',
			data : {
				noticeId : row.noticeId
			},
			dataType : 'json',
			cache : false,
			success : function(response) {
				$.messager.progress('close');
				if (response && response.context) {
					showCdescDialog.find('div[name=cdesc]').html(response.context);
					showCdescDialog.dialog('open');
				} else {
					$.messager.alert('提示', '没有公告/公文内容！', 'error');
				}
			}
		});
		datagrid.datagrid('reload');
	}
</script>
</head>
<body class="easyui-layout">
	<div region="center" border="false">
		<table id="datagrid"></table>
	</div>

	<!-- 不加会出错 不明白原因 -->
	<div id="menu" class="easyui-menu" style="width:120px;display: none;">
		<div onclick="add();" iconCls="icon-add">增加</div>
		<div onclick="del();" iconCls="icon-remove">删除</div>
		<div onclick="edit();" iconCls="icon-edit">编辑</div>
	</div>

	<div id="showCdescDialog" style="display: none;overflow: auto;width: 500px;height: 400px;">
		<div name="cdesc"></div>
	</div>
</body>
</html>