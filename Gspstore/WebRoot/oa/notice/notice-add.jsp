<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<style>
.huibd {
	border: 1px solid #000000;
}

td {
	white-space: nowrap;
	font-size: 12px;
}
</style>
<link rel="stylesheet"
	href="<%=basePath%>/main/include/css/button-usr.css" type="text/css"></link>
<script type="text/javascript"
	src="<%=basePath%>/main/include/js/form_valid.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/jslib/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#doReturn").click(function(){
		 window.parent.window.closePayoutDialog();
		});
	$("#doReturn").attr("disabled",false);
	$(window.parent.document.getElementById("loading")).fadeOut("slow",function(){
	     $(this).css("display","none");
	});
	noticeAddForm = $('#noticeAddForm').form({
			url : '${pageContext.request.contextPath}/noticeAction!add.action',
			success : function(data) {
				var json = $.parseJSON(data);
				if (json && json.success) {
					$.messager.show({
						title : '成功',
						msg : json.msg
					});
					$("#doReturn").trigger("click"); 
				} else {
					$.messager.show({
						title : '失败',
						msg : json.msg
					});
				}
			}
		});
		
		var cdescAdd;
		cdescAdd = $('#cdescAdd').xheditor({
			tools : 'Cut,Copy,Paste,Print,|,Fontface,FontSize,Bold,Italic,Underline,Strikethrough,FontColor,Removeformat,|,Align,List,Outdent,Indent,|,Link,Unlink,Img,|,Hr,Table,Fullscreen',
			html5Upload : true,
			upMultiple : 4,
			upLinkUrl : '${pageContext.request.contextPath}/noticeAction!upload.action',
			upLinkExt : 'zip,rar,txt,doc,docx,xls,xlsx',
			upImgUrl : '${pageContext.request.contextPath}/noticeAction!upload.action',
			upImgExt : 'jpg,jpeg,gif,png',
			width:500,
			height:200
		});
	});
</script>
</head>
<body>
<div align="center" style="padding: 10px;">
	<form id="noticeAddForm" method="post">
		<table class="tableForm" onsubmit="return addReceivedUserIds()">
			<tr>
				<td class="tdLeft">标题</td>
				<td class="tdRight">
				<input name="title" type="text" maxlength="100" class="easyui-validatebox" data-options="required:true" missingMessage="请填写标题" style="width: 430px;"/>
				</td>
			</tr>
			<tr>
				<td class="tdLeft"><lable title="早于当前时间立即发布" class="easyui-tooltip">发布时间</label></td>
				<td class="tdRight"><input name="releaseTime" type="text" 
					class="easyui-datetimebox" data-options="required:true"
					missingMessage="请选择发布时间<br/>早于当前时间立即发布" style="width: 140px;" />
					&nbsp;失效时间&nbsp;<input name="expiryTime" type="text"
					class="easyui-datetimebox" data-options="required:true"
					missingMessage="请选择失效时间" style="width: 140px;" />
					&nbsp;类型&nbsp;<select id="isAnnouncement" name="isAnnouncement" type="text" class="easyui-combobox"
						data-options="required:true,panelHeight:45,editable:false" missingMessage="请选择类型" style="width: 100px;">
						<option value="0">通知</option>
						<option value="1">公告</option>
						<option value="2">公文</option>
		 			</select>
		 		</td>
			</tr>
			<tr>
				<td class="tdLeft">接收人</td>
				<td class="tdRight" >
					<input id="usercombotree" class="easyui-combotree" style="width:200px;" name="receivedUserIds" missingMessage="请选择接收人"
            			data-options="multiple:true,url:'${pageContext.request.contextPath}/roleAction!tree.action',required:true" />
		 		</td>
			</tr>
			<tr>
				<td class="tdLeft">优先次序</td>
				<td class="tdRight"><input name="priority" class="easyui-numberbox" missingMessage="优先次序只能是数字" type="text" style="width: 50px;"/>
				&nbsp;备注&nbsp;<input name="remark" type="text" style="width: 300px;"/></td>
			</tr>
			<tr>
				<td>内容</td>
				<td>
					<textarea name="context" id="cdescAdd"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="30" align="center">
					<input value=" 关 闭 " type="button" id="doReturn" /> 
					<input value=" 完 成 " type="submit" />
				</td>
			</tr>
		</table>
	</form>
</div>
</body>
</html>