<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<style>
.huibd {
	border: 1px solid #000000;
}

td {
	white-space: nowrap;
	font-size: 12px;
}
</style>
<link rel="stylesheet"
	href="<%=basePath%>/main/include/css/button-usr.css" type="text/css"></link>
<script type="text/javascript"
	src="<%=basePath%>/main/include/js/form_valid.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/jslib/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#doReturn").click(function(){
			 window.parent.window.closePayoutDialog();
			});
		$("#doReturn").attr("disabled",false);
		$(window.parent.document.getElementById("loading")).fadeOut("slow",function(){
		     $(this).css("display","none");
		});
		
		noticeEditForm = $('#noticeEditForm').form({
				url : '${pageContext.request.contextPath}/noticeAction!edit.action',
				success : function(data) {
					var json = $.parseJSON(data);
					if (json && json.success) {
						$.messager.show({
							title : '成功',
							msg : json.msg
						});
						$("#doReturn").trigger("click"); 
					} else {
						$.messager.show({
							title : '失败',
							msg : json.msg
						});
					}
				}
			});
			
			var cdescEdit;
			cdescEdit = $('#cdescEdit').xheditor({
				tools : 'Cut,Copy,Paste,Print,|,Fontface,FontSize,Bold,Italic,Underline,Strikethrough,FontColor,Removeformat,|,Align,List,Outdent,Indent,|,Link,Unlink,Img,|,Hr,Table,Fullscreen',
				html5Upload : true,
				upMultiple : 4,
				upLinkUrl : '${pageContext.request.contextPath}/noticeAction!upload.action',
				upLinkExt : 'zip,rar,txt,doc,docx,xls,xlsx',
				upImgUrl : '${pageContext.request.contextPath}/noticeAction!upload.action',
				upImgExt : 'jpg,jpeg,gif,png',
				width:500,
				height:200
			});
			// 将内容中的双引号替换为\",否则显示的时候会出问题
			cdescEdit.setSource("${fn:replace(notice.context,'\"','\\\"')}");
			
			if("${error}".length>0){
				alert("${error}");
				//$.messager.alert('提示', '${error}', 'error');
				window.parent.window.closePayoutDialog();
			}
		});
</script>
<script type="text/javascript">
function initReceivedUserIds(){
	console.info("combox:"+$('#userIdscombox'));
	console.info("options");
	console.info($('#userIdscombox').combo('options'));
	//console.info($('#userIdscombox').combo('setValues','1,2'.split(',')));
	//$('#userIdscombox').combo('setValues','超级用户,2'.split(','));
	var arr = document.getElementById("oldReceivedIds").value.split(',');
	console.info(arr);
	for (i=0;i<arr.length ;i++ ){
        node=$('#userIdscombox').combotree('tree').tree('find',arr[i]);
        $('#userIdscombox').combotree('tree').tree('check',node.target);
        //$('#userIdscombox').combotree('tree').tree('expandAll', node.target);
	}
}
</script>
</head>
<body>
<div align="center">
	<form id="noticeEditForm" method="post">
		<table class="tableForm">
			<tr>
				<td class="tdLeft">标题</td>
				<td class="tdRight">
				<input name="title" value="${notice.title }" type="text" maxlength="100" readonly="readonly" style="width: 400px;"/>
				</td>
			</tr>
			<tr>
				<td class="tdLeft">发布时间</td>
				<td class="tdRight"><input name="releaseTime" type="text"
					class="easyui-datetimebox" data-options="required:true" value="${notice.releaseTime }"
					missingMessage="请选择发布时间" style="width: 140px;" />
					&nbsp;失效时间&nbsp;<input name="expiryTime" type="text"
					class="easyui-datetimebox" data-options="required:true" value="${notice.expiryTime }"
					missingMessage="请选择失效时间" style="width: 140px;" />
					&nbsp;类型&nbsp;<select id="isAnnouncement" name="isAnnouncement" type="text" class="easyui-combobox"
						data-options="required:true,panelHeight:45,editable:false" missingMessage="请选择类型" style="width: 100px;">
						<option value="0" <c:if test="${notice.isAnnouncement==0 }">selected="selected"</c:if> >通知</option>
						<option value="1" <c:if test="${notice.isAnnouncement==1 }">selected="selected"</c:if> >公告</option>
						<option value="2" <c:if test="${notice.isAnnouncement==2 }">selected="selected"</c:if> >公文</option>
		 			</select>
		 		</td>
			</tr>
			<tr>
				<td class="tdLeft">接收人</td>
				<td class="tdRight">
					<input id="oldReceivedIds" value="${notice.receivedUserIds }"/>
				${notice.receivedUserNames}<input id="userIdscombox" class="easyui-combotree" style="width:200px;" name="receivedUserIds" missingMessage="请选择接收人"
            			data-options="multiple:true,url:'${pageContext.request.contextPath}/roleAction!tree.action',required:true,
            			lines : !sy.isLessThanIe8(),onLoadSuccess : function(row, data) {
							<%-- var t = $(this);
							console.info(data);
							console.info(${notice.receivedUserIds }); --%>
							if (data) {
								<%-- $(data).each(function(index, d) {
									if (this.state == 'closed') {
										t.tree('expandAll');
									}
									if(this.id == '0'){
										this.checked = true;
									}
									console.info(this);
								});
								console.info('1,2'.split(',')); --%>
								initReceivedUserIds();
							}
						}" />
		 		</td>
			</tr>
			<tr>
				<td class="tdLeft">优先次序</td>
				<td class="tdRight">
					<input name="priority" class="easyui-numberbox" missingMessage="优先次序只能是数字" type="text" style="width: 50px;" value="${notice.priority }"/>
		 			&nbsp;备注&nbsp;<input name="remark" value="${notice.remark }" type="text" />
		 		</td>
			</tr>
			<tr>
				<td class="tdLeft">内容</td>
				<td class="tdRight">
					<textarea name="context" id="cdescEdit"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="30" align="center">
					<input value=" 关 闭 " type="button" id="doReturn" /> 
					<input value=" 完 成 " type="submit" />
				</td>
			</tr>
		</table>
	</form>
</div>
</body>
</html>