<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<script type="text/javascript" charset="utf-8">
    var searchForm;
	var datagrid;
	var showCdescDialog;
	var iframeDialog;
	//update-begin author:anchao for:防止form重复提交  date：20130310
	var formFlag = true;
	//update-end author:anchao for:防止form重复提交  date：20130310
	$(function() {
	    //查询列表	
	    searchForm = $('#searchForm').form();
		datagrid = $('#datagrid').datagrid({
			url : 'noticeAction!datagrid.action',
			title : '公告通知管理列表',
			iconCls : 'icon-save',
			pagination : true,
			pagePosition : 'bottom',
			rownumbers : true,
			pageSize : 10,
			pageList : [ 10, 20, 30, 40 ],
			fit : true,
			fitColumns : true,
			nowrap : true,
			border : false,
			idField : 'title',
			sortName : 'releaseTime',
			sortOrder : 'desc',
			columns : [ [ 
			{field:'ck',checkbox:true,
						formatter:function(value,row,index){
							return row.title;
						}
					},
			   {field:'title',title:'标题',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.title;
					}
				},				
			   {field:'releaseTime',title:'发布时间',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.releaseTime;
					}
				},				
			   {field:'releaseUserId',title:'发布人ID【不能据此排序】',align:'center',sortable:false,
					formatter:function(value,row,index){
						return row.releaseUserId;
					}
				},				
			   {field:'releaseUserName',title:'发布人姓名【不能据此排序】',align:'center',sortable:false,
					formatter:function(value,row,index){
						return row.releaseUserName;
					}
				},				
			   {field:'depart',title:'发布部门',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.depart;
					}
				},				
			   {field:'typeinTime',title:'录入时间',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.typeinTime;
					}
				},				
			   {field:'computerIp',title:'计算机IP地址',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.computerIp;
					}
				},				
			   {field:'expiryTime',title:'失效时间',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.expiryTime;
					}
				},				
			   {field:'priority',title:'优先次序',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.priority;
					}
				},				
			   {field:'isAnnouncement',title:'类型',align:'center',sortable:true,
					formatter:function(value,row,index){
						if(row.isAnnouncement==0){
							return "通知";
						}else if(row.isAnnouncement==1){
							return "公告";
						}else{
							return "公文";
						}
					}
				},		
			   {field:'remark',title:'备注',align:'center',sortable:true,
					formatter:function(value,row,index){
						return row.remark;
					}
				},
				{field:'context',title:'查看',align:'center',sortable:true,
					formatter:function(value,row,index){
						return '<span class="icon-search" style="display:inline-block;vertical-align:middle;width:16px;height:16px;"></span><a href="javascript:void(0);" onclick="showCdesc(' + index + ');">查看详细</a>';
					}
				}
			 ] ],
			toolbar : [ {
				text : '增加',
				iconCls : 'icon-add',
				handler : function() {
					add();
				}
			}, '-', {
				text : '删除',
				iconCls : 'icon-remove',
				handler : function() {
					del();
				}
			}, '-', {
				text : '修改',
				iconCls : 'icon-edit',
				handler : function() {
					edit();
				}
			}, '-', {
				text : '取消选中',
				iconCls : 'icon-undo',
				handler : function() {
					datagrid.datagrid('unselectAll');
				}
			}, '-' ],
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
		});

		showCdescDialog = $('#showCdescDialog').show().dialog({
			title : '公告通知管理描述',
			modal : true,
			closed : true,
			maximizable : true
		});
		
		iframeDialog = $('#iframeDialog').show().dialog({
			title : 'POP弹出页面',
			modal : true,
			closed : true,
			maximizable : false,
			onClose : function(){
			    document.getElementById("iframe").src = "about:blank";
			    datagrid.datagrid('reload');
			}
		});
	});

	function _search() {
		datagrid.datagrid('load', sy.serializeObject(searchForm));
	}
	function cleanSearch() {
		datagrid.datagrid('load', {});
		searchForm.find('input').val('');
	}
	
	//update-begin author:gaoxingang for:优化将添加和修改页面分开  date：20130309
	function add() {
		if(iframeDialog==undefined){
			$.messager.alert('提示', '出错了，请关闭浏览器重试！', 'error');
		}
		$('#iframeDialog').dialog('setTitle', '添加公告通知'); 
		//$('#iframeDialog').dialog('open');
		iframeDialog.dialog('open'); //有时候会报错
		$("#loading").css("display","block");
		document.getElementById("iframe").src = "${pageContext.request.contextPath}/oa/notice/notice-add.jsp";
	
	}
	//update-end author:gaoxingang for:优化将添加和修改页面分开  date：20130309
	
	function del() {
		var rows = datagrid.datagrid('getSelections');
		var ids = [];
		if (rows.length > 0) {
			$.messager.confirm('请确认', '您要删除当前所选项目？', function(r) {
				if (r) {
					for ( var i = 0; i < rows.length; i++) {
						if(new Date(rows[i].releaseTime.replace(/-/g,"/"))-new Date()<0&&new Date(rows[i].expiryTime.replace(/-/g,"/"))-new Date()>0){
							$.messager.alert('提示', '标题为【'+rows[i].title+'】的信息已发布且未失效不能删除！', 'error');
							return;
						}
						ids.push(rows[i].title);
					}
					$.ajax({
						url : 'noticeAction!delete.action',
						data : {
							ids : ids.join(',')
						},
						dataType : 'json',
						success : function(response) {
							datagrid.datagrid('load');
							datagrid.datagrid('unselectAll');
							$.messager.show({
								title : '提示',
								msg : '删除成功！'
							});
						}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择要删除的记录！', 'error');
		}
	}
	
	//update-begin author:gaoxingang for:优化将添加和修改页面分开  date：20130309
	function edit() {
		var rows = datagrid.datagrid('getSelections');
		if (rows.length == 1) {
			if(iframeDialog==undefined){
				$.messager.alert('提示', '出错了，请关闭浏览器重试！', 'error');
				return;
			}
			if(new Date(rows[0].releaseTime.replace(/-/g,"/"))-new Date()<0){
				$.messager.alert('提示', '该信息已发布不能修改！', 'error');
				return;
			}
			if(new Date(rows[0].expiryTime.replace(/-/g,"/"))-new Date()<0){
				$.messager.alert('提示', '该信息已失效不能修改！', 'error');
				return;
			}
			$('#iframeDialog').dialog('setTitle', '编辑公告通知 --【'+rows[0].title+'】'); 
			iframeDialog.dialog('open');
			$("#loading").css("display","block");
			document.getElementById("iframe").src = "${pageContext.request.contextPath}/noticeAction!toEdit.action?title=" + encodeURI(rows[0].title);
		} else if (rows.length > 1) {
			$.messager.alert('提示', '同一时间只能编辑一条记录！', 'error');
		} else {
			$.messager.alert('提示', '请选择一条要编辑的记录！', 'error');
		}
	}
	//update-end author:gaoxingang for:优化将添加和修改页面分开  date：20130309
	
	function showCdesc(index) {
		var rows = datagrid.datagrid('getRows');
		var row = rows[index];
		$.messager.progress({
			text : '数据加载中....',
			interval : 100
		});
		$.ajax({
			url : 'noticeAction!showCdesc.action',
			data : {
				title : row.title
			},
			dataType : 'json',
			cache : false,
			success : function(response) {
				$.messager.progress('close');
				if (response && response.context) {
					showCdescDialog.find('div[name=cdesc]').html(response.context);
					showCdescDialog.dialog('open');
				} else {
					$.messager.alert('提示', '没有内容！', 'error');
				}
			}
		});
		datagrid.datagrid('unselectAll');
	}
</script>
</head>
<body class="easyui-layout">
	<div region="north" border="false" title="过滤条件" collapsed="true"  style="height: 60px;overflow: hidden;display: none;" align="left">
		<form id="searchForm">
			<table class="tableForm datagrid-toolbar" style="width: 100%;height: 100%;">
				<tr>
					<td>标题</td>
					<td><input name="title" style="width:120px;" /></td>
					<td>内容</td>
					<td><input name="context" style="width:120px;" /><td>
					<!-- <td>接收人Ids</td>
					<td><input name="receivedUserIds" style="width:120px;" /><td> -->
					<td>录入时间</td>
					<td>
						<input id="beginTypeinTime" name="beginTypeinTime" style="width:117px;" class="easyui-datebox"/>
					   	<input id="endTypeinTime" name="endTypeinTime" style="width:117px;" class="easyui-datebox"/>
					<td>
					<td>发布时间</td>
					<td>
						<input id="beginReleaseTime" name="beginReleaseTime" style="width:117px;" class="easyui-datebox"/>
					   	<input id="endReleaseTime" name="endReleaseTime" style="width:117px;" class="easyui-datebox"/>
					<td>
					<td>失效时间</td>
					<td>
						<input id="beginExpiryTime" name="beginExpiryTime" style="width:117px;" class="easyui-datebox"/>
					   	<input id="endExpiryTime" name="endExpiryTime" style="width:117px;" class="easyui-datebox"/>
					<td>
				    <td><a href="javascript:void(0);" class="easyui-linkbutton" onclick="_search();">过滤</a><a href="javascript:void(0);" class="easyui-linkbutton" onclick="cleanSearch();">取消</a></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div region="center" border="false">
		<table id="datagrid"></table>
	</div>

	<div id="menu" class="easyui-menu" style="width:120px;display: none;">
		<div onclick="add();" iconCls="icon-add">增加</div>
		<div onclick="del();" iconCls="icon-remove">删除</div>
		<div onclick="edit();" iconCls="icon-edit">编辑</div>
	</div>

	<div id="showCdescDialog" style="display: none;overflow: auto;width: 500px;height: 400px;">
		<div name="cdesc"></div>
	</div>
	
	<div id="iframeDialog" style="display: none;overflow: hidden;width: 620px;height: 430px;">
		<div id="loading"  style="position: absolute;z-index: 10000;
          top:30;left:0;width:100%;height:95%;background: #DDDDDB  url('<%=request.getContextPath()%>/css/images/loading.gif') no-repeat center ;text-align:center;">
        </div>
		<iframe name="iframe" id="iframe" src=""  scrolling="auto" frameborder="0" style="width:100%;height:100%;">
	    </iframe>
    </div>
</body>
</html>