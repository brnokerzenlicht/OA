<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<style type="text/css">
   .tdLeft{
      width:30%;
      text-align: right;
   }
   .tdRight{
      width:70%;
      text-align: left;
   }
   .tableForm{
   	  width:80%;
   	  text-align: center;
   }
</style>
<script type="text/javascript" charset="utf-8">
    var searchForm;
	var datagrid;
	var taskAssignForm;
	var taskAssignDialog;
	var iframeDialog;
	var formFlag = true;
	$(function() {
	    //查询列表	
	    searchForm = $('#searchForm').form();
		datagrid = $('#datagrid').datagrid({
			url : 'taskAction!datagrid.action',
			title : '任务信息列表',
			iconCls : 'icon-save',
			pagination : true,
			pagePosition : 'bottom',
			rownumbers : true,
			pageSize : 10,
			pageList : [ 10, 20, 30, 40 ],
			fit : true,
			fitColumns : false,
			nowrap : true,
			singleSelect:false,
			border : false,
			idField : 'taskid',
			sortName : 'taskStartDate',
			sortOrder : 'desc',
			columns : [ [ 
			{field:'ck',checkbox:true,
						formatter:function(value,row,index){
							return row.taskid;
						}
					},
			   {field:'taskid',title:'任务ID',align:'center',sortable:false,width:90,
					formatter:function(value,row,index){
						return row.taskid;
					}
				},				
			   {field:'taskname',title:'任务名称',align:'center',sortable:false,width:90,
					formatter:function(value,row,index){
						return row.taskname;
					}
				},				
			   {field:'taskDescription',title:'任务描述',align:'center',sortable:false,width:150,
					formatter:function(value,row,index){
						return row.taskDescription;
					}
				},				
			   {field:'taskAssignee',title:'任务执行者',align:'center',sortable:false,width:90,
					formatter:function(value,row,index){
						return row.taskAssigneeName;
					}
				},				
			   {field:'procid',title:'所在流程ID',align:'center',sortable:false,width:100,
					formatter:function(value,row,index){
						return row.procid;
					}
				},				
			   {field:'procname',title:'所在流程名称',align:'center',sortable:false,width:100,
					formatter:function(value,row,index){
						return row.procname;
					}
				},				
			   {field:'startUser',title:'所在流程发起人',align:'center',sortable:false,width:100,
					formatter:function(value,row,index){
						return row.startUser;
					}
				},				
			   {field:'startDate',title:'所在流程开始时间',align:'center',sortable:false,width:100,
					formatter:function(value,row,index){
						return dateFormatYMD(row.startDate);
					}
				},				
			   {field:'taskStartDate',title:'任务开始时间',align:'center',sortable:false,width:100,
					formatter:function(value,row,index){
						return dateFormatYMD(row.taskStartDate);
					}
				},				
			   {field:'taskAssignDate',title:'任务签收时间',align:'center',sortable:false,width:100,
					formatter:function(value,row,index){
						return dateFormatYMD(row.taskAssignDate);
					}
				}	
			 ] ],
			toolbar : [ {
				text : '指定执行者',
				iconCls : 'icon-add',
				handler : function() {
					assign();
				}
			}, '-', {
				text : '完成任务',
				iconCls : 'icon-remove',
				handler : function() {
					complete();
				}
			}, '-', {
				text : '取消选中',
				iconCls : 'icon-undo',
				handler : function() {
					datagrid.datagrid('unselectAll');
				}
			}, '-' ],
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
		});

		taskAssignForm = $('#taskAssignForm').form({
			url : 'taskAction!assign.action',
			success : function(data) {
				var json = $.parseJSON(data);
				if (json && json.success) {
					$.messager.show({
						title : '成功',
						msg : json.msg
					});
					datagrid.datagrid('reload');
					datagrid.datagrid('unselectAll');
					taskAssignDialog.dialog('close');
				} else {
					$.messager.show({
						title : '失败',
						msg : json.msg
					});
				}
				formFlag = true ;
			}
		});

		taskAssignDialog = $('#taskAssignDialog').show().dialog({
			title : '指定任务执行者',
			modal : true,
			closed : true,
			maximizable : true,
			buttons : [ {
				text : '确定',
				handler : function() {
				  if (taskAssignForm.form('validate')){
					if(formFlag){
						formFlag = false ;
						taskAssignForm.submit();
					}
				  }
				}
			} ]
		});

		iframeDialog = $('#iframeDialog').show().dialog({
			title : 'POP弹出页面',
			modal : true,
			closed : true,
			maximizable : true,
			onClose : function(){
			    document.getElementById("iframe").src = "about:blank";
			    datagrid.datagrid('reload');
			    datagrid.datagrid('unselectAll');
			}
		});
	});

	function _search() {
		datagrid.datagrid('load', sy.serializeObject(searchForm));
	}
	function cleanSearch() {
		datagrid.datagrid('load', {});
		searchForm.find('input').val('');
	}
	function assign() {
		var rows = datagrid.datagrid('getSelections');
		if (rows.length == 1) {
			$.messager.progress({
				text : '数据加载中....',
				interval : 100
			});
			$.ajax({
				url : 'taskAction!showDescAssign.action',
				data : {
					taskid : rows[0].taskid
				},
				dataType : 'json',
				cache : false,
				success : function(response) {
					taskAssignForm.find('input,textarea').val('');
					taskAssignForm.form('load', response);
					$('div.validatebox-tip').remove();
					$('#taskAssignForm_taskAssignee_combox').combobox({
						data : response.candidateUsers,
						valueField:'userid',
						textField:'username'
					});
					taskAssignDialog.dialog('open');
					$.messager.progress('close');
				}
			});
		} else {
			$.messager.alert('提示', '请选择一项要指定执行者的任务！', 'error');
		}
	}
	function complete() {
		var rows = datagrid.datagrid('getSelections');
		if (rows.length == 1) {
			if(rows[0].taskAssigneeName==null||rows[0].taskAssigneeName.trim().length==0){
				$.messager.alert('提示', '请先指定执行者！', 'error');
				return;
			}
			if(rows[0].taskAssigneeName!=document.getElementById("myName").value){
				$.messager.alert('提示', '执行者不是自己，不能执行！', 'error');
				return;
			}
			$('#iframeDialog').dialog('setTitle', '完成任务 --【'+rows[0].taskname+'】'); 
			//$('#iframeDialog').dialog('open');
			iframeDialog.dialog('open'); //有时候会报错
			$("#loading").css("display","block");
			document.getElementById("iframe").src = rows[0].formKey+"?procid="+rows[0].procid+"&taskid="+rows[0].taskid+"&taskAssignee="+rows[0].taskAssignee;
		} else {
			$.messager.alert('提示', '请选择一项要完成的任务！', 'error');
		}
		/* var rows = datagrid.datagrid('getSelections');
		if (rows.length == 1) {
			$.messager.progress({
				text : '数据加载中....',
				interval : 100
			});
			$.ajax({
				url : 'taskAction!showDesc.action',
				data : {
					taskid : rows[0].taskid
				},
				dataType : 'json',
				cache : false,
				success : function(response) {
					taskCompleteForm.find('input,textarea').val('');
					taskCompleteForm.form('load', response);
					$('div.validatebox-tip').remove();
					taskCompleteDialog.dialog('open');
					$.messager.progress('close');
				}
			});
		} else {
			$.messager.alert('提示', '请选择一项要完成的任务！', 'error');
		} */
	}
	
</script>
</head>
<body class="easyui-layout">
	<input type="hidden" id="myName" value="${sessionScope.sessionInfo.realName }"/>
	<div region="north" border="false" title="过滤条件" collapsed="true"  style="height: 60px;overflow: hidden;display: none;" align="left">
		<form id="searchForm">
			<table class="tableForm datagrid-toolbar" style="width: 100%;height: 100%;">
				<tr>
					<td>任务开始时间</td>
					<td>
						<input id="beginTaskStartDate" name="beginTaskStartDate" style="width:117px;" class="easyui-datebox"/>
					   	<input id="endTaskStartDate" name="endTaskStartDate" style="width:117px;" class="easyui-datebox"/>
					</td>
			    <td><a href="javascript:void(0);" class="easyui-linkbutton" onclick="_search();">过滤</a><a href="javascript:void(0);" class="easyui-linkbutton" onclick="cleanSearch();">取消</a></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div region="center" border="false">
		<table id="datagrid"></table>
	</div>

	<div id="menu" class="easyui-menu" style="width:120px;display: none;">
		<div onclick="assign();" iconCls="icon-add">指定执行者</div>
		<div onclick="complete()" iconCls="icon-remove">完成任务</div>
	</div>

	<div id="taskAssignDialog" style="display: none;width: 530px;height: 280px;padding:10px;" align="center">
		<form id="taskAssignForm" method="post">
			<!-- <input type="hidden" name="taskid" /> -->
			<table class="tableForm">
				<tr>
					<td class="tdLeft">任务ID</td>
					<td class="tdRight"><input name="taskid" type="text"
						maxlength="20" readonly="readonly" style="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">任务名称</td>
					<td class="tdRight"><input name="taskname" type="text"
						maxlength="20" readonly="readonly" style="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">任务描述</td>
					<td class="tdRight"><input name="taskDescription" type="text"
						maxlength="20" readonly="readonly" style="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">任务开始时间</td>
					<td class="tdRight"><input name="taskStartDate" type="text"
						maxlength="20"readonly="readonly" style="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">指定执行者名称</td>
					<td class="tdRight">
						<select id="taskAssignForm_taskAssignee_combox" name="taskAssignee" type="text" class="easyui-combobox"
							data-options="required:true,panelHeight:80,editable:false" missingMessage="指定执行者"  style="width: 120px;">
		 				</select>
		 			</td>
				</tr>
			</table>
		</form>
	</div>
	
	<div id="iframeDialog" style="display: none;overflow: hidden;width: 600px;height: 500px;">
		<div id="loading"  style="position: absolute;z-index: 10000;
          top:30;left:0;width:100%;height:95%;background: #DDDDDB  url('<%=request.getContextPath()%>/css/images/loading.gif') no-repeat center ;text-align:center;">
        </div>
		<iframe name="iframe" id="iframe" src=""  scrolling="auto" frameborder="0" style="width:100%;height:100%;">
	    </iframe>
	</div>
</body>
</html>