<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
	<style>
	.huibd {
		border: 1px solid #000000;
			}
	td {
		white-space: nowrap;
		font-size: 12px;
		}
	</style>
<link rel="stylesheet" href="<%=basePath%>/main/include/css/button-usr.css" type="text/css"></link>
<script language="javascript" type="text/javascript" src="<%=basePath%>/main/include/js/form_valid.js"></script>
<script language="javascript" type="text/javascript" src="<%=basePath%>/jslib/datepicker/WdatePicker.js"></script>
<link rel="stylesheet" href="<%=basePath%>/main/css/tablelock.css" mce_href="tablelock.css" /> 
<script language="javascript" type="text/javascript" src="<%=basePath%>/jslib/LodopFuncs.js"></script>
<script type="text/javascript" charset="utf-8">
	$(function() {
		taskCompleteForm = $('#taskCompleteForm').form({
			url : 'taskAction!complete.action',
			success : function(data) {
				var json = $.parseJSON(data);
				if (json && json.success) {
					$.messager.show({
						title : '成功',
						msg : json.msg
					});
					datagrid.datagrid('reload');
					taskCompleteDialog.dialog('close');
				} else {
					$.messager.show({
						title : '失败',
						msg : '操作失败！'+json.msg
					});
				}
				formFlag = true ;
			}
		});
	});
	$(document).ready(function() {
		//$.fn.TableLock({table:'stockinDetail_table',lockRow:0,lockColumn:0,width:'960px',height:'100%'});
	$("#doReturn").click(function(){
		 window.parent.window.closePayoutDialog();
		});
	$("#doReturn").attr("disabled",false);
	$(window.parent.document.getElementById("loading")).fadeOut("slow",function(){
         $(this).css("display","none");
    });
});
</script>
</head>
<body class="easyui-layout">
	<div style="display: none;width: 530px;height: 255px;padding:10px;" align="center">
		<form id="taskCompleteForm" method="post">
			<input type="hidden" name="taskid" />
			<table class="tableForm">
				<tr>
					<td class="tdLeft">任务ID</td>
					<td class="tdRight"><input type="text" readonly="readonly" name="taskid" id="taskid" value="${taskPage.taskid }"/></td>
				</tr>
				<tr>
					<td class="tdLeft">任务名称</td>
					<td class="tdRight"><input type="text" readonly="readonly" name="taskname" id="taskname" value="${taskPage.taskname }"/></td>
				</tr>
				<tr>
					<td class="tdLeft">任务描述</td>
					<td class="tdRight"><input type="text" readonly="readonly" name="taskDescription" id="taskDescription" value="${taskPage.taskDescription }"/></td>
				</tr>
				<tr>
					<td class="tdLeft">任务开始时间</td>
					<td class="tdRight"><input name="taskStartDate" type="text"
						maxlength="20" class="easyui-datebox" value="${taskPage.taskStartDate }"
						data-options="editable:false" style="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">任务执行者</td>
					<td class="tdRight"><input name="taskAssignee" type="text"
						maxlength="20" class="easyui-validatebox" value="${taskPage.taskAssignee }"
						data-options="required:true,editable:false" missingMessage="请先签收再执行" missstyle="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">已有变量${taskPage.candidateUsers }</td>
					<c:forEach items="taskPage.hadVars" var="hadVar">
					<td class="tdRight">
						${taskPage.hadVars}
		 			</td>
					</c:forEach>
				</tr>
				<tr>
					<td class="tdLeft">需要的变量</td>
					<td class="tdRight">
		 			</td>
				</tr>
			</table>
		</form>
		<div align="center">
				<table>
                  <tr>
                    <td height="50" colspan="8"  align="center">
                        <input value=" 关 闭 " type="button" id="doReturn"/>
                         <input value=" 完 成 " type="submit" id="doComplete"/>
                    </td>
                  </tr>
                 </table>
		</div>
	</div>
</body>
</html>