<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<style>
.huibd {
	border: 1px solid #000000;
}

td {
	white-space: nowrap;
	font-size: 12px;
}
</style>
<link rel="stylesheet"
	href="<%=basePath%>/main/include/css/button-usr.css" type="text/css"></link>
<script type="text/javascript"
	src="<%=basePath%>/main/include/js/form_valid.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/jslib/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$("#doReturn").click(function(){
	 window.parent.window.closePayoutDialog();
	});
$("#doReturn").attr("disabled",false);
$(window.parent.document.getElementById("loading")).fadeOut("slow",function(){
     $(this).css("display","none");
});
	taskCompleteForm = $('#taskCompleteForm').form({
		url : 'taskAction!complete.action',
		success : function(data) {
			var json = $.parseJSON(data);
			if (json && json.success) {
				$.messager.show({
					title : '成功',
					msg : json.msg
				});
				$("#doReturn").trigger("click"); 
			} else {
				$.messager.show({
					title : '失败',
					msg : '操作失败！'+json.msg
				});
			}
		}
	});
});
</script>
</head>
<body>
	<div style="width: 530px; padding: 10px;" align="center">
		<form id="taskCompleteForm" method="post">
			<input type="hidden" name="taskPage.taskid" />
			<table class="tableForm">
				<tr>
					<td class="tdLeft">任务ID</td>
					<td class="tdRight"><input type="text" readonly="readonly" name="taskid" id="taskid" value="${taskPage.taskid }"/></td>
				</tr>
				<tr>
					<td class="tdLeft">任务名称</td>
					<td class="tdRight"><input type="text" readonly="readonly" name="taskname" id="taskname" value="${taskPage.taskname }"/></td>
				</tr>
				<tr>
					<td class="tdLeft">任务描述</td>
					<td class="tdRight"><input type="text" readonly="readonly" name="taskDescription" id="taskDescription" value="${taskPage.taskDescription }"/></td>
				</tr>
				<tr>
					<td class="tdLeft">任务开始时间</td>
					<td class="tdRight"><input name="taskStartDate" type="text" readonly="readonly"
						maxlength="20" value='<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${taskPage.taskStartDate }"/>' 
						style="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">任务执行者</td>
					<td class="tdRight"><input name="taskAssignee" type="text" readonly="readonly"
						maxlength="20" class="easyui-validatebox" value="${taskPage.taskAssignee }"
						data-options="required:true" missingMessage="请先签收再执行" missstyle="width: 155px;" /></td>
				</tr>
				<s:if test="taskPage.hadVars.size >0">
					<tr><td colspan="2">已有内容</td></tr>
					<s:iterator value="taskPage.hadVars" var="hadVar" status="stuts">
						<tr>
							<td class="tdLeft">${hadVar.id}</td>
							<td class="tdRight"><input type="text" readonly="readonly" name="${hadVar.id}" id="${hadVar.id}"
								<s:if test="#hadVar.type=='Date' ">
									value='<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${hadVar.value }"/>' 
								</s:if>
								<s:elseif test="#hadVar.type=='Boolean'">
									<s:if test="#hadVar.value">
										value="同意"
									</s:if>
									<s:else>
										value="不同意"
									</s:else>
								</s:elseif>
								<s:else>
							 		value="${hadVar.value}"
								</s:else>
							 /></td>
						</tr>
					</s:iterator>
				</s:if>
				<s:if test="taskPage.needVars.size > 0">
					<tr><td colspan="2">请填写</td></tr>
					<s:iterator value="taskPage.needVars" var="needVar" status="stuts">
						<tr>
							<td class="tdLeft">${needVar.name}</td>
							<td class="tdRight">
								<s:if test="#needVar.type=='date'">
									<input type="text" name="${needVar.id}" id="${needVar.id}" class="easyui-datebox"/>
								</s:if>
								<s:elseif test="#needVar.type=='boolean'">
									<select id="${needVar.id }" name="${needVar.id }" type="text" class="easyui-combobox"
							data-options="required:true,panelHeight:45,editable:false" missingMessage="请选择一项" style="width: 120px;">
										<option value="true">同意</option>
										<option value="false">不同意</option>
									</select>
								</s:elseif>
								<s:else>
							 		<input type="text" name="${needVar.id}" id="${needVar.id}"/>
								</s:else>
							</td>
						</tr>
					</s:iterator>
				</s:if>
				<tr>
					<td height="50" colspan="8" align="center">
						<input value=" 关 闭 " type="button" id="doReturn" /> 
						<input value=" 完 成 " type="submit" id="doComplete" />
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>