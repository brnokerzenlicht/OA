<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<style type="text/css">
   .tdLeft{
      width:15%;
      text-align: right;
   }
   .tdRight{
      width:35%;
      text-align: left;
   }
</style>
<script type="text/javascript" charset="utf-8">
    var searchForm;
	var datagrid;
	var fileAddDialog;
	var fileAddForm;
	var formFlag = true;
	$(function() {
	    //查询列表	
	    searchForm = $('#searchForm').form();
		datagrid = $('#datagrid').datagrid({
			url : 'fileAction!datagrid.action',
			title : '文件信息列表',
			iconCls : 'icon-save',
			pagination : true,
			pagePosition : 'bottom',
			rownumbers : true,
			pageSize : 10,
			pageList : [ 10, 20, 30, 40 ],
			fit : true,
			fitColumns : false,
			nowrap : true,
			border : false,
			idField : 'fileid',
			singleSelect:true,
			sortName : 'uploadTime',
			sortOrder : 'desc',
			columns : [ [ 
			{field:'ck',checkbox:true,
						formatter:function(value,row,index){
							return row.fileid;
						}
					},
			   {field:'filename',title:'文件名称',align:'center',sortable:true,width:90,
					formatter:function(value,row,index){
						return row.filename;
					}
				},				
			   {field:'uploadTime',title:'上传时间',align:'center',sortable:true,width:40,
					formatter:function(value,row,index){
						return dateFormatYMD(row.uploadTime);
					}
				},				
			   {field:'fileSize',title:'文件大小',align:'center',sortable:true,width:40,
					formatter:function(value,row,index){
						return row.fileSize;
					}
				},				
			   {field:'depart',title:'部门',align:'center',sortable:true,width:80,
					formatter:function(value,row,index){
						return row.depart;
					}
				},				
			   {field:'classification',title:'文件类型',align:'center',sortable:true,width:90,
					formatter:function(value,row,index){
						return row.classification;
					}
				},				
			   {field:'remark',title:'备注',align:'center',sortable:true,width:90,
					formatter:function(value,row,index){
						return row.remark;
					}
				},				
			   {field:'filePath',title:'下载',align:'center',sortable:true,width:80,
					formatter:function(value,row,index){
						return '<a href="'+row.filePath+'">下载</a>';
					}
				}			
			 ] ],
			toolbar : [ {
				text : '上传文件',
				iconCls : 'icon-add',
				handler : function() {
					add();
				}
			}, '-', {
				text : '删除文件',
				iconCls : 'icon-remove',
				handler : function() {
					del();
				}
			}, '-', {
				text : '取消选中',
				iconCls : 'icon-undo',
				handler : function() {
					datagrid.datagrid('unselectAll');
				}
			}, '-' ],
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
		});

		fileAddForm = $('#fileAddForm').form({
			url : 'fileAction!add.action',
			success : function(data) {
				var json = $.parseJSON(data);
				if (json && json.success) {
					$.messager.show({
						title : '成功',
						msg : json.msg
					});
					datagrid.datagrid('reload');
					fileAddDialog.dialog('close');
				} else {
					$.messager.show({
						title : '失败',
						msg : json.msg
					});
				}
				formFlag = true ;
			}
		});

		fileAddDialog = $('#fileAddDialog').show().dialog({
			title : '上传文件',
			modal : true,
			closed : true,
			maximizable : true,
			buttons : [ {
				text : '添加',
				handler : function() {
				  if (fileAddForm.form('validate')){
					if(formFlag){
						formFlag = false ;
						fileAddForm.submit();
					}
				  }
				}
			} ]
		});

		cdescAdd = $('#cdescAdd').xheditor({
			tools : 'Link',
			html5Upload : true,
			upMultiple : 4,
			upLinkUrl : 'fileAction!upload.action',
			upLinkExt : 'zip,rar,txt,doc,docx,xls,xlsx',
			upImgUrl : 'fileAction!upload.action',
			upImgExt : 'jpg,jpeg,gif,png'
		});
	});

	function _search() {
		datagrid.datagrid('load', sy.serializeObject(searchForm));
	}
	function cleanSearch() {
		datagrid.datagrid('load', {});
		searchForm.find('input').val('');
	}
	function add() {
		fileAddForm.find('input,textarea').val('');
		$('div.validatebox-tip').remove();
		fileAddDialog.dialog('open');
	}
	function del() {
		var rows = datagrid.datagrid('getSelections');
		var ids = [];
		if (rows.length > 0) {
			$.messager.confirm('请确认', '您要删除当前所选项目？', function(r) {
				if (r) {
					for ( var i = 0; i < rows.length; i++) {
						ids.push(rows[i].fileid);
					}
					$.ajax({
						url : 'fileAction!delete.action',
						data : {
							ids : ids.join(',')
						},
						dataType : 'json',
						success : function(response) {
							datagrid.datagrid('load');
							datagrid.datagrid('unselectAll');
							$.messager.show({
								title : '提示',
								msg : response.msg
							});
						}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择要删除的记录！', 'error');
		}
	}
</script>
</head>
<body class="easyui-layout">
	<div region="north" border="false" title="过滤条件" collapsed="true"  style="height: 60px;overflow: hidden;display: none;" align="left">
		<form id="searchForm">
			<table class="tableForm datagrid-toolbar" style="width: 100%;height: 100%;">
				<tr>
					<td>文件名称</td>
					<td><input name="filename" style="width:120px;" class="easyui-validatebox"/></td>
					<td>部门</td>
					<td><input name="depart" style="width:120px;" class="easyui-validatebox"/></td>
					<td>文件类型</td>
					<td><input name="classification" style="width:120px;" class="easyui-validatebox"/></td>
					<td>备注</td>
					<td><input name="remark" style="width:120px;" class="easyui-validatebox"/></td>
					<td>上传时间</td>
					<td>
						<input name="beginUploadTime" style="width:120px;" class="easyui-datebox"/>
						<input name="endUploadTime" style="width:120px;" class="easyui-datebox"/>
					</td>
			    <td><a href="javascript:void(0);" class="easyui-linkbutton" onclick="_search();">过滤</a><a href="javascript:void(0);" class="easyui-linkbutton" onclick="cleanSearch();">取消</a></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div region="center" border="false">
		<table id="datagrid"></table>
	</div>

	<div id="menu" class="easyui-menu" style="width:120px;display: none;">
		<div onclick="add();" iconCls="icon-add">上传文件</div>
		<div onclick="del();" iconCls="icon-remove">删除</div>
	</div>

	<div id="fileAddDialog" style="display: none;width: 530px;height: 255px;padding:10px;" align="center">
		<form id="fileAddForm" method="post">
			<table class="tableForm">
				<tr>
					<td class="tdLeft">文件名称</td>
					<td class="tdRight"><input name="filename" type="text"
						maxlength="20" class="easyui-validatebox"
						data-options="required:true" missingMessage="请填写文件名称"
						style="width: 155px;" /></td>
					<td class="tdLeft">部门</td>
					<td class="tdRight"><select id="depart" name="depart" type="text"
						class="easyui-combobox"
						data-options="required:true,
		 						        url : 'dictParamAction!dictCombobox.action?paramLevel=001',
						                valueField : 'paramValue',
						                textField : 'paramName',
						                editable:false
		 						       "
						missingMessage="请选择部门" style="width: 159px;">
					</select></td>
				</tr>
				<tr>
					<td class="tdLeft">文件类型</td>
					<td class="tdRight"><select id="classification" name="classification" type="text"
						class="easyui-combobox"
						data-options="required:true,
		 						        url : 'dictParamAction!dictCombobox.action?paramLevel=001',
						                valueField : 'paramValue',
						                textField : 'paramName',
						                editable:false
		 						       "
						missingMessage="请选择文件类型" style="width: 159px;">
					</select></td>
					<td class="tdLeft">备注</td>
					<td class="tdRight"><input name="remark" type="text"
						class="easyui-validatebox" style="width: 159px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">上传文件</td>
					<td class="tdRight"><input type="file" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>