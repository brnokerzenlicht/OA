<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<style>
.huibd {
	border: 1px solid #000000;
}

td {
	white-space: nowrap;
	font-size: 12px;
}
</style>
<link rel="stylesheet"
	href="<%=basePath%>/main/include/css/button-usr.css" type="text/css"></link>
<script type="text/javascript"
	src="<%=basePath%>/main/include/js/form_valid.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/jslib/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$("#doReturn").click(function(){
	 window.parent.window.closePayoutDialog();
	});
$("#doReturn").attr("disabled",false);
$(window.parent.document.getElementById("loading")).fadeOut("slow",function(){
     $(this).css("display","none");
});
	vacateReStartForm = $('#vacateReStartForm').form({
		url : 'vacateAction!reStart.action',
		success : function(data) {
			var json = $.parseJSON(data);
			if (json && json.success) {
				$.messager.show({
					title : '成功',
					msg : json.msg
				});
				$("#doReturn").trigger("click"); 
			} else {
				$.messager.show({
					title : '失败',
					msg : json.msg
				});
			}
		}
	});
});
</script>
</head>
<body>
	<div style="width: 530px; padding: 10px;" align="center">
		<form id="vacateReStartForm" method="post">
			<input type="hidden" name="vacateid" value="${vacatePage.vacateid }" />
			<table class="tableForm">
				<tr>
					<td class="tdLeft">任务ID</td>
					<td class="tdRight"><input type="text" readonly="readonly" name="taskid" id="taskid" value="${vacatePage.taskid }"/></td>
				</tr>
				<tr>
					<td class="tdLeft">任务执行者</td>
					<td class="tdRight"><input name="taskAssignee" type="text" readonly="readonly"
						maxlength="20" class="easyui-validatebox" value="${vacatePage.taskAssignee }"
						data-options="required:true" missingMessage="请先签收再执行" missstyle="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">请假ID</td>
					<td class="tdRight"><input name="procid" type="text" readonly="readonly"
						maxlength="20" value="${vacatePage.procid }"
						missstyle="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">请假申请人</td>
					<td class="tdRight"><input name="vacatename" type="text" readonly="readonly"
						maxlength="20" value="${vacatePage.vacatename }"
						missstyle="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">开始时间</td>
					<td class="tdRight"><input name="startDate" type="text" class="easyui-datebox" data-options="required:true"
						missingMessage="请选择开始日期" 
						maxlength="20" value='<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${vacatePage.startDate }"/>' /></td>
				</tr>
				<tr>
					<td class="tdLeft">结束时间</td>
					<td class="tdRight"><input name="endDate" type="text" class="easyui-datebox" data-options="required:true"
						missingMessage="请选择结束日期" 
						maxlength="20" value='<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${vacatePage.endDate }"/>' /></td>
				</tr>
				<tr>
					<td class="tdLeft">请假原因</td>
					<td class="tdRight"><input name="reason" type="text"
						data-options="required:true" missingMessage="请填写请假原因"
						maxlength="20" class="easyui-validatebox" value="${vacatePage.reason }"
						missstyle="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">部门负责人审批结果</td>
					<td class="tdRight">
						<s:if test="vacatePage.approve">
							同意
						</s:if>
						<s:else>
							不同意
						</s:else>
					</td>
				</tr>
				<tr>
					<td class="tdLeft">部门负责人审批意见</td>
					<td class="tdRight">
						<input name="approveSuggestion" type="text" readonly="readonly" value="${vacatePage.approveSuggestion }"/>
					</td>
				</tr>
				<s:if test="vacatePage.approve">
					<tr>
						<td class="tdLeft">办公室审批结果</td>
						<td class="tdRight">不同意<input name="reApprove" type="hidden" value="${vacatePage.reApprove }"/>
						</td>
					</tr>
					<tr>
						<td class="tdLeft">办公室审批意见</td>
						<td class="tdRight">
							<input name="reApproveSuggestion" type="hidden" value="${vacatePage.reApproveSuggestion }"/>
						</td>
					</tr>
				</s:if>
				<tr>
					<td height="50" colspan="8" align="center">
						<input value=" 关 闭 " type="button" id="doReturn" /> 
						<input value=" 完 成 " type="submit" id="doComplete" />
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>