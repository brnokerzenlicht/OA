<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/inc.jsp"></jsp:include>
<style type="text/css">
   .tdLeft{
      width:15%;
      text-align: right;
   }
   .tdRight{
      width:35%;
      text-align: left;
   }
</style>
<script type="text/javascript" charset="utf-8">
    var searchForm;
	var datagrid;
	var vacateAddDialog;
	var vacateAddForm;
	var formFlag = true;
	$(function() {
	    //查询列表	
	    searchForm = $('#searchForm').form();
		datagrid = $('#datagrid').datagrid({
			url : 'vacateAction!datagrid.action',
			title : '请假信息列表',
			iconCls : 'icon-save',
			pagination : true,//显示分页  
			pagePosition : 'bottom',
			rownumbers : true,
			pageSize : 10,
			pageList : [ 10, 20, 30, 40 ],
			fit : true,
			fitColumns : false,
			singleSelect:false,
			nowrap : true,
			border : false,
			idField : 'procid',
			sortName : 'vacatename',
			sortOrder : 'asc',
			columns : [ [ 
				{field:'ck',checkbox:true,
						formatter:function(value,row,index){
							return row.procid;
						}
					},
			   {field:'vacatename',title:'姓名',align:'center',sortable:false,width:90,
					formatter:function(value,row,index){
						return row.vacatename;
					}
				},				
			   {field:'startDate',title:'开始时间',align:'center',sortable:false,width:100,
					formatter:function(value,row,index){
						return dateFormatYMD(row.startDate);
					}
				},				
			   {field:'endDate',title:'结束时间',align:'center',sortable:false,width:100,
					formatter:function(value,row,index){
						return dateFormatYMD(row.endDate);
					}
				},				
			   {field:'reason',title:'请假原因',align:'center',sortable:false,width:300,
					formatter:function(value,row,index){
						return row.reason;
					}
				}			
			 ] ],
			toolbar : [ {
				text : '增加',
				iconCls : 'icon-add',
				handler : function() {
					add();
				}
			}, '-', {
				text : '删除',
				iconCls : 'icon-remove',
				handler : function() {
					del();
				}
			}, '-', {
				text : '取消选中',
				iconCls : 'icon-undo',
				handler : function() {
					datagrid.datagrid('unselectAll');
				}
			}, '-' ],
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
		});

		vacateAddForm = $('#vacateAddForm').form({
			url : 'vacateAction!add.action',
			success : function(data) {
				var json = $.parseJSON(data);
				if (json && json.success) {
					$.messager.show({
						title : '成功',
						msg : json.msg
					});
					datagrid.datagrid('reload');
					vacateAddDialog.dialog('close');
				} else {
					$.messager.show({
						title : '失败',
						msg : json.msg
					});
				}
				formFlag = true ;
			}
		});

		vacateAddDialog = $('#vacateAddDialog').show().dialog({
			title : '添加请假信息',
			modal : true,
			closed : true,
			maximizable : true,
			buttons : [ {
				text : '添加',
				handler : function() {
				  if (vacateAddForm.form('validate')){
					if(formFlag){
						formFlag = false ;
						vacateAddForm.submit();
					}
				  }
				}
			} ]
		});
	});

	function _search() {
		datagrid.datagrid('load', sy.serializeObject(searchForm));
	}
	function cleanSearch() {
		datagrid.datagrid('load', {});
		searchForm.find('input').val('');
	}
	function add() {
		vacateAddForm.find('input,textarea').val('');
		$('div.validatebox-tip').remove();
		vacateAddDialog.dialog('open');
	}
	function del() {
		var rows = datagrid.datagrid('getSelections');
		var ids = [];
		if (rows.length > 0) {
			$.messager.confirm('请确认', '您要删除当前所选项目？', function(r) {
				if (r) {
					for ( var i = 0; i < rows.length; i++) {
						ids.push(rows[i].procid);
					}
					$.ajax({
						url : 'vacateAction!delete.action',
						data : {
							ids : ids.join(',')
						},
						dataType : 'json',
						success : function(response) {
							datagrid.datagrid('load');
							datagrid.datagrid('unselectAll');
							$.messager.show({
								title : '提示',
								msg : response.msg
							});
						}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择要删除的记录！', 'error');
		}
	}
	
</script>
</head>
<body class="easyui-layout">
	<div region="north" border="false" title="过滤条件——只能精确匹配，暂不支持模糊查询" collapsed="true"  style="height: 60px;overflow: hidden;display: none;" align="left">
		<form id="searchForm">
			<table class="tableForm datagrid-toolbar" style="width: 100%;height: 100%;">
				<tr>
					<td>姓名</td>
					<td><input name="vacatename" style="width:120px;" class="easyui-validatebox"/></td>
					<td>开始时间</td>
					<td>
						<input id="beginStartDate" name="beginStartDate" style="width:117px;" class="easyui-datebox"/>
					   	<input id="endStartDate" name="endStartDate" style="width:117px;" class="easyui-datebox"/>
					</td>
					<td>结束时间</td>
					<td>
						<input id="beginStartDate" name="beginStartDate" style="width:117px;" class="easyui-datebox"/>
					   	<input id="endStartDate" name="endStartDate" style="width:117px;" class="easyui-datebox"/>
					</td>
					<td>请假原因</td>
					<td><input name="reason" style="width:120px;" class="easyui-validatebox"/></td>
			    <td><a href="javascript:void(0);" class="easyui-linkbutton" onclick="_search();">过滤</a><a href="javascript:void(0);" class="easyui-linkbutton" onclick="cleanSearch();">取消</a></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div region="center" border="false">
		<table id="datagrid"></table>
	</div>

	<div id="menu" class="easyui-menu" style="width:120px;display: none;">
		<div onclick="add();" iconCls="icon-add">增加</div>
		<div onclick="del();" iconCls="icon-remove">删除</div>
	</div>


	<div id="vacateAddDialog" style="display: none;width: 530px;height: 255px;padding:10px;" align="center">
		<form id="vacateAddForm" method="post">
			<table class="tableForm">
				<tr>
					<td class="tdLeft">姓名</td>
					<td class="tdRight">
					${sessionScope.sessionInfo.realName }
					<!-- 不生效已在后台添加 <input name="vacatename" type="text"
						maxlength="20" class="easyui-validatebox"
						data-options="required:true" missingMessage="请填写姓名"
						style="width: 155px;" /> --></td>
				</tr>
				<tr>
					<td class="tdLeft">开始时间</td>
					<td class="tdRight"><input name="startDate" type="text"
						maxlength="" class="easyui-datebox" data-options="required:true"
						missingMessage="请填写开始日期" style="width: 159px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">结束时间</td>
					<td class="tdRight"><input name="endDate" type="text"
						maxlength="" class="easyui-datebox" data-options="required:true"
						missingMessage="请填写结束日期" style="width: 159px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">请假原因</td>
					<td class="tdRight"><input name="reason" type="text"
						maxlength="15" class="easyui-validatebox"
						data-options="required:true" missingMessage="请填写请假原因"
						style="width: 155px;" /></td>
				</tr>
			</table>
		</form>
	</div>

	<div id="vacateEditDialog" style="display: none;width: 530px;height: 255px;padding:10px;" align="center">
		<form id="vacateEditForm" method="post">
			<input type="hidden" name="procid" />
			<table class="tableForm">
				<tr>
					<td class="tdLeft">姓名</td>
					<td class="tdRight"><input name="vacatename" type="text"
						maxlength="20" class="easyui-validatebox"
						data-options="required:true" missingMessage="请填写姓名"
						style="width: 155px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">开始时间</td>
					<td class="tdRight"><input name="startDate" type="text"
						maxlength="" class="easyui-datebox" data-options="required:true"
						missingMessage="请选择开始日期" style="width: 159px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">结束时间</td>
					<td class="tdRight"><input name="endDate" type="text"
						maxlength="" class="easyui-datebox" data-options="required:true"
						missingMessage="请选择结束日期" style="width: 159px;" /></td>
				</tr>
				<tr>
					<td class="tdLeft">请假原因</td>
					<td class="tdRight"><input name="reason" type="text"
						maxlength="15" class="easyui-validatebox"
						data-options="required:true" missingMessage="请填写请假原因"
						style="width: 155px;" /></td>
				</tr>	
			</table>
		</form>
	</div>
</body>
</html>