package cn.neu.action;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import cn.neu.page.HistoryProcPage;
import cn.neu.service.HistoryProcServiceI;

import com.jeecg.action.BaseAction;
import com.jeecg.pageModel.Json;
import com.jeecg.util.ExceptionUtil;
import com.opensymphony.xwork2.ModelDriven;

/**   
 * @Title: Action
 * @Description: 历史流程包含历史任务
 */
@SuppressWarnings("serial")
@Action(value = "historyProcAction", results = { @Result(name = "historyProcStarted", location = "/oa/historyProc/historyProcStarted.jsp"),
												 @Result(name = "historyProcInvolved", location = "/oa/historyProc/historyProcInvolved.jsp"),
												 @Result(name = "detail", location = "/oa/historyProc/detail.jsp")})
public class HistoryProcAction extends BaseAction implements ModelDriven<HistoryProcPage> {

	private static final Logger logger = Logger.getLogger(HistoryProcAction.class);

	private HistoryProcPage historyProcPage = new HistoryProcPage();
	private HistoryProcServiceI historyProcService;
	
	public HistoryProcServiceI getHistoryProcService() {
		return historyProcService;
	}
	@Autowired
	public void setHistoryProcService(HistoryProcServiceI historyProcService) {
		this.historyProcService = historyProcService;
	}
	public HistoryProcPage getModel() {
		return historyProcPage;
	}
	/**
	 * 跳转到历史流程页面
	 * @throws Exception 
	 */
	public String goHistoryProc() throws Exception {
		if(historyProcPage.getProcType().equals("started")){
			return "historyProcStarted";
		}else if(historyProcPage.getProcType().equals("involved")){
			return "historyProcInvolved";
		}else{
			throw new Exception("页面未找到！");
		}
	}
	/**
	 * 跳转到查看详细信息页面
	 * @return
	 */
	public String detail() {
		logger.info("detail"+historyProcPage);
		historyProcPage = historyProcService.get(historyProcPage.getProcid());
		logger.info("detail"+historyProcPage.getDiagramPath());
		return "detail";
	}
	/**
	 * 获得pageHotel数据表格
	 */
	public void datagrid() {
		logger.info("datagrid"+historyProcPage.getProcType());
		writeJson(historyProcService.datagrid(historyProcPage,getSessionInfo().getLoginName()));
		logger.info("datagrid"+historyProcPage.getProcType());
	}
	/**
	 * 获得无分页的所有数据
	 */
	public void combox(){
		logger.info("combox"+historyProcPage.getProcType());
		writeJson(historyProcService.listAll(historyProcPage,getSessionInfo().getLoginName()));
		logger.info("combox"+historyProcPage.getProcType());
	}
	/**
	 * 删除一个历史流程
	 */
	public void delete(){
		Json j = new Json();
		try {
			logger.info("delete"+historyProcPage.getIds());
			historyProcService.delete(historyProcPage.getIds());
			j.setSuccess(true);
			j.setMsg("删除成功！");
		} catch (Exception e) {
			if(e.getLocalizedMessage().indexOf("is still running")>0){
				j.setMsg("流程正在执行，无法删除！");
				logger.error(ExceptionUtil.getExceptionMessage(e));
			}else{
				j.setMsg("删除失败！"+e.getMessage());
			}
		}
		writeJson(j);
	}
	
	public final HistoryProcPage getHistoryProcPage() {
		return historyProcPage;
	}
	public final void setHistoryProcPage(HistoryProcPage historyProcPage) {
		this.historyProcPage = historyProcPage;
	}
}
