package cn.neu.action;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletInputStream;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;

import cn.neu.page.NoticePage;
import cn.neu.service.NoticeServiceI;

import com.jeecg.action.BaseAction;
import com.jeecg.pageModel.Json;
import com.jeecg.service.UserServiceI;
import com.jeecg.util.ExceptionUtil;
import com.jeecg.util.ResourceUtil;
import com.opensymphony.xwork2.ModelDriven;

/**   
 * @Title: Action
 * @Description: 公告通知
 */
@Action(value = "noticeAction", results = { @Result(name = "notice", location = "/oa/notice/notice.jsp") ,
											@Result(name = "myNotice", location = "/oa/notice/myNotice.jsp") ,
											@Result(name = "myInform", location = "/oa/notice/myInform.jsp") ,
		  							    	@Result(name = "notice-edit", location = "/oa/notice/notice-edit.jsp") })
public class NoticeAction extends BaseAction implements ModelDriven<NoticePage> {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(NoticeAction.class);

	private NoticeServiceI noticeService;
	
	private UserServiceI userService;
	
	private NoticePage noticePage = new NoticePage();

	public NoticePage getModel() {
		return noticePage;
	}


	public NoticeServiceI getNoticeService() {
		return noticeService;
	}

	@Autowired
	public void setNoticeService(NoticeServiceI noticeService) {
		this.noticeService = noticeService;
	}


	public final UserServiceI getUserService() {
		return userService;
	}

	@Autowired
	public final void setUserService(UserServiceI userService) {
		this.userService = userService;
	}


	/**
	 * 跳转到公告管理页面
	 * 
	 * @return
	 */
	public String goNotice() {
		return "notice";
	}
	
	/**
	 * 跳转到我的公告/公文页面
	 * 
	 * @return
	 */
	public String goMyNotice() {
		return "myNotice";
	}
	/**
	 * 跳转到我的通知页面
	 * 
	 * @return
	 */
	public String goMyInform() {
		return "myInform";
	}

	/**
	 * 跳转到查看desc页面
	 * 
	 * @return
	 * @throws Exception 
	 */
	public void showCdesc() throws Exception {
		logger.info("showCdesc"+noticePage);
		try {
			writeJson(noticeService.get(noticePage));
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * 阅读页面
	 * @return
	 * @throws Exception 
	 */
	public void readNotice() throws Exception {
		logger.info("readNotice"+noticePage);
		try {
			writeJson(noticeService.getByNoticeId(noticePage.getNoticeId()));
		} catch (Exception e) {
			throw e;
		}
	}
	
	//add-begin author:gaoxingang for:优化将添加和修改页面分开  date：20130309
	/**
	 * 跳转到查看edit页面
	 * 
	 * @return
	 * @throws Exception 
	 */
	public String toEdit(){
		try {
			// 解决get中文乱码问题
			String title = new String(noticePage.getTitle().getBytes("ISO-8859-1"),"UTF-8");
			getRequest().setAttribute("notice", noticeService.get(title));
		} catch (Exception e) {
			getRequest().setAttribute("error", e.getMessage());
		}
		return "notice-edit";
	}
	//add-end author:gaoxingang for:优化将添加和修改页面分开  date：20130309
	
	/**
	 * 获得pageHotel数据表格
	 */
	public void datagrid() {
		logger.info("datagrid before"+noticePage);
		writeJson(noticeService.datagrid(noticePage));
		logger.info("datagrid after"+noticePage);
	}

	/**
	 * 获得pageHotel数据表格
	 * @throws Exception 
	 */
	public void datagridUnGroup() throws Exception {
		logger.warn("datagridUnGroup"+noticePage);
		writeJson(noticeService.datagridUnGroup(noticePage,getSessionInfo().getUserId()));
	}

	/**
	 * 获得无分页的所有数据
	 */
	public void combox(){
		writeJson(noticeService.listAll(noticePage));
	}

	/**
	 * 添加一个公告通知
	 */
	public void add() {
		Json j = new Json();
		try {
			logger.info("add"+noticePage);
			if(noticePage.getReleaseTime().after(noticePage.getExpiryTime())){
				throw new Exception("发布时间必须早于失效时间！");
			}
			Date now = new Date();
			if(noticePage.getReleaseTime().before(now)){
				// 发布时间比当前时间早的自动改为当前时间
				noticePage.setReleaseTime(now);
//				throw new Exception("发布时间必须晚于当前时间！");
			}
			noticePage.setTypeinTime(now);
			noticePage.setComputerIp(getSessionInfo().getIp());
			String userId = getSessionInfo().getUserId();
			String dept = userService.findById(userId).getOrgName();
			if(dept==null){
				dept = "未指定部门";
			}
			noticePage.setDepart(dept);
			noticePage.setReleaseUserId(userId);
			noticeService.add(noticePage);
			j.setSuccess(true);
			j.setMsg("添加成功！");
		} catch (Exception e) {
			j.setMsg("添加失败！"+e.getMessage());
			logger.error(ExceptionUtil.getExceptionMessage(e));
		}
		writeJson(j);
	}

	/**
	 * 编辑公告通知
	 */
	public void edit() {
		Json j = new Json();
		try {
			logger.info("edit"+noticePage);
			if(noticePage.getReleaseTime().after(noticePage.getExpiryTime())){
				throw new Exception("发布时间必须早于失效时间！");
			}
			Date now = new Date();
			if(noticePage.getReleaseTime().before(now)){
				throw new Exception("发布时间必须晚于当前时间！");
			}
			noticePage.setTypeinTime(now);
			noticePage.setComputerIp(getSessionInfo().getIp());
			String userId = getSessionInfo().getUserId();
			String dept = userService.findById(userId).getOrgName();
			if(dept==null){
				dept = "未指定部门";
			}
			noticePage.setDepart(dept);
			noticePage.setReleaseUserId(userId);
			noticeService.update(noticePage);
			j.setSuccess(true);
			j.setMsg("编辑成功！");
		} catch (Exception e) {
			logger.error(ExceptionUtil.getExceptionMessage(e));
			j.setMsg("编辑失败！"+e.getMessage());
		}
		writeJson(j);
	}

	/**
	 * 删除公告通知
	 */
	public void delete() {
		Json j = new Json();
		logger.info("delete"+noticePage);
		noticeService.delete(noticePage.getIds());
		j.setSuccess(true);
		writeJson(j);
	}

	/**
	 * 文件上传
	 */
	public void upload() {
		String savePath = ServletActionContext.getServletContext().getRealPath("/") + ResourceUtil.getUploadDirectory() + "/";// 文件保存目录路径
		String saveUrl = "/" + ResourceUtil.getUploadDirectory() + "/";// 文件保存目录URL

		String contentDisposition = ServletActionContext.getRequest().getHeader("Content-Disposition");// 如果是HTML5上传文件，那么这里有相应头的

		if (contentDisposition != null) {// HTML5拖拽上传文件
			Long fileSize = Long.valueOf(ServletActionContext.getRequest().getHeader("Content-Length"));// 上传的文件大小
			String fileName = contentDisposition.substring(contentDisposition.lastIndexOf("filename=\""));// 文件名称
			fileName = fileName.substring(fileName.indexOf("\"") + 1);
			fileName = fileName.substring(0, fileName.indexOf("\""));

			ServletInputStream inputStream;
			try {
				inputStream = ServletActionContext.getRequest().getInputStream();
			} catch (IOException e) {
				uploadError("上传文件出错！");
				ExceptionUtil.getExceptionMessage(e);
				return;
			}

			if (inputStream == null) {
				uploadError("您没有上传任何文件！");
				return;
			}

			if (fileSize > ResourceUtil.getUploadFileMaxSize()) {
				uploadError("上传文件超出限制大小！", fileName);
				return;
			}

			// 检查文件扩展名
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			if (!Arrays.<String> asList(ResourceUtil.getUploadFileExts().split(",")).contains(fileExt)) {
				uploadError("上传文件扩展名是不允许的扩展名。\n只允许" + ResourceUtil.getUploadFileExts() + "格式！");
				return;
			}

			savePath += fileExt + "/";
			saveUrl += fileExt + "/";

			SimpleDateFormat yearDf = new SimpleDateFormat("yyyy");
			SimpleDateFormat monthDf = new SimpleDateFormat("MM");
			SimpleDateFormat dateDf = new SimpleDateFormat("dd");
			Date date = new Date();
			String ymd = yearDf.format(date) + "/" + monthDf.format(date) + "/" + dateDf.format(date) + "/";
			savePath += ymd;
			saveUrl += ymd;

			File uploadDir = new File(savePath);// 创建要上传文件到指定的目录
			if (!uploadDir.exists()) {
				uploadDir.mkdirs();
			}

			String newFileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + fileExt;// 新的文件名称
			File uploadedFile = new File(savePath, newFileName);

			try {
				FileCopyUtils.copy(inputStream, new FileOutputStream(uploadedFile));
			} catch (FileNotFoundException e) {
				uploadError("上传文件出错！");
				ExceptionUtil.getExceptionMessage(e);
				return;
			} catch (IOException e) {
				uploadError("上传文件出错！");
				ExceptionUtil.getExceptionMessage(e);
				return;
			}

			uploadSuccess(ServletActionContext.getRequest().getContextPath() + saveUrl + newFileName, fileName, 0);// 文件上传成功

			return;
		}

		MultiPartRequestWrapper multiPartRequest = (MultiPartRequestWrapper) ServletActionContext.getRequest();// 由于struts2上传文件时自动使用了request封装
		File[] files = multiPartRequest.getFiles(ResourceUtil.getUploadFieldName());// 上传的文件集合
		String[] fileNames = multiPartRequest.getFileNames(ResourceUtil.getUploadFieldName());// 上传文件名称集合

		if (files == null || files.length < 1) {
			uploadError("您没有上传任何文件！");
			return;
		}

		for (int i = 0; i < files.length; i++) {// 循环所有文件
			File file = files[i];// 上传的文件(临时文件)
			String fileName = fileNames[i];// 上传文件名

			if (file.length() > ResourceUtil.getUploadFileMaxSize()) {
				uploadError("上传文件超出限制大小！", fileName);
				return;
			}

			// 检查文件扩展名
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			if (!Arrays.<String> asList(ResourceUtil.getUploadFileExts().split(",")).contains(fileExt)) {
				uploadError("上传文件扩展名是不允许的扩展名。\n只允许" + ResourceUtil.getUploadFileExts() + "格式！");
				return;
			}

			savePath += fileExt + "/";
			saveUrl += fileExt + "/";

			SimpleDateFormat yearDf = new SimpleDateFormat("yyyy");
			SimpleDateFormat monthDf = new SimpleDateFormat("MM");
			SimpleDateFormat dateDf = new SimpleDateFormat("dd");
			Date date = new Date();
			String ymd = yearDf.format(date) + "/" + monthDf.format(date) + "/" + dateDf.format(date) + "/";
			savePath += ymd;
			saveUrl += ymd;

			File uploadDir = new File(savePath);// 创建要上传文件到指定的目录
			if (!uploadDir.exists()) {
				uploadDir.mkdirs();
			}

			String newFileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + fileExt;// 新的文件名称
			File uploadedFile = new File(savePath, newFileName);

			try {
				FileCopyUtils.copy(file, uploadedFile);// 利用spring的文件工具上传
			} catch (Exception e) {
				uploadError("上传文件失败！", fileName);
				return;
			}

			uploadSuccess(ServletActionContext.getRequest().getContextPath() + saveUrl + newFileName, fileName, i);// 文件上传成功

		}

	}

	private void uploadError(String err, String msg) {
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("err", err);
		m.put("msg", msg);
		writeJson(m);
	}

	private void uploadError(String err) {
		uploadError(err, "");
	}

	private void uploadSuccess(String newFileName, String fileName, int id) {
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("err", "");
		Map<String, Object> nm = new HashMap<String, Object>();
		nm.put("url", newFileName);
		nm.put("localfile", fileName);
		nm.put("id", id);
		m.put("msg", nm);
		writeJson(m);
	}
	
	public NoticePage getNoticePage() {
		return noticePage;
	}


	public void setNoticePage(NoticePage noticePage) {
		this.noticePage = noticePage;
	}
}
