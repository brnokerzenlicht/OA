package cn.neu.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.jeecg.action.BaseAction;

import cn.neu.page.NoticePage;

@ServerEndpoint(value = "/noticeServer", configurator = NoticeServerConfigurator.class)
public class NoticeServer {
	private Session session;
	private static List<Session> users = Collections.synchronizedList(new ArrayList<Session>());
	private static final Logger logger = Logger.getLogger(NoticeServer.class);
	
	@OnOpen
	public void startChatChannel(EndpointConfig config, Session session) {
		this.session = session;
		
		// 初次登陆取得数据
		try {
			List<NoticePage> list = BaseAction.noticeService.getByUserId((String)session.getUserProperties().get("userId"));
			for (NoticePage noticePage : list) {
				session.getBasicRemote().sendText(JSON.toJSONStringWithDateFormat(noticePage, "yyyy-MM-dd HH:mm:ss"));
			}
		} catch (Exception e) {
			logger.error("发送数据出错"+e.getMessage());
			e.printStackTrace();
		}
		users.add(session);
	}

	// 给所有用户推送消息
	public static void sendToAll(NoticePage noticePage) {
		for (Session session : users) {
			if (session.isOpen()) {
				if(noticePage.getReceivedUserIds().contains((String) session.getUserProperties().get("userId"))){
					try {
						session.getBasicRemote().sendText(JSON.toJSONStringWithDateFormat(noticePage, "yyyy-MM-dd HH:mm:ss"));
						logger.info("send "+noticePage+" to "+session.getUserProperties().get("userId"));
					} catch (IOException e) {
						logger.error("Error sending message to " + session.getUserProperties().get("userId") + e.getMessage());
					}
				}
			}
		}
	}

	@OnError
	public void myError(Throwable t) {
		logger.error("Error: " + t.getMessage());
	}

	@OnClose
	public void closeChannel() {
		users.remove(this.session);
	}
}
