package cn.neu.action;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

public class NoticeServerConfigurator extends ServerEndpointConfig.Configurator {
	
	@Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        sec.getUserProperties().put("userId", request.getParameterMap().get("userId").get(0));
    }
}

