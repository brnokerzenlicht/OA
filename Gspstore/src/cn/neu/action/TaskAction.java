package cn.neu.action;

import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import cn.neu.page.TaskPage;
import cn.neu.service.MyTaskServiceI;

import com.jeecg.action.BaseAction;
import com.jeecg.pageModel.Json;
import com.jeecg.util.ExceptionUtil;
import com.opensymphony.xwork2.ModelDriven;

/**   
 * @Title: Action
 * @Description: 我的任务包含指定任务的执行者
 */
@SuppressWarnings("serial")
@Action(value = "taskAction", results = { @Result(name = "task", location = "/oa/task/task.jsp")})
public class TaskAction extends BaseAction implements ModelDriven<TaskPage> {

	private static final Logger logger = Logger.getLogger(TaskAction.class);

	private TaskPage taskPage = new TaskPage();
	private MyTaskServiceI myTaskService;
	
	public MyTaskServiceI getTaskService() {
		return myTaskService;
	}
	@Autowired
	public void setMyTaskService(MyTaskServiceI myTaskService) {
		this.myTaskService = myTaskService;
	}
	public TaskPage getModel() {
		return taskPage;
	}
	/**
	 * 跳转到我的任务管理页面
	 */
	public String goTask() {
		return "task";
	}
//	/**
//	 * 跳转到查看详细信息页面用于完成任务
//	 * @return
//	 */
//	public void showDesc() {
//		taskPage = myTaskService.get(taskPage.getTaskid());
//		System.out.println(taskPage);
//		myTaskService.getCandidateUsers(taskPage);
//		System.out.println(taskPage.getHadVars());
//	}
	/**
	 * 跳转到查看详细信息页面用于指定执行者		可能需要再加一个jsp页面和完成任务类似
	 * @return
	 */
	public void showDescAssign() {
		TaskPage result = myTaskService.get(taskPage.getTaskid());
		logger.info(result);
		myTaskService.getCandidateUsers(result);
//		logger.info(result.getHadVars());
		writeJson(result);
	}
	/**
	 * 获得pageHotel数据表格
	 */
	public void datagrid() {
		writeJson(myTaskService.datagrid(taskPage,getSessionInfo().getLoginName()));
	}
	/**
	 * 获得无分页的所有数据
	 */
	public void combox(){
		writeJson(myTaskService.listAll(taskPage,getSessionInfo().getLoginName()));
	}
//	/**
//	 * 完成一个任务
//	 */
//	public void complete(){
//		Json j = new Json();
//		try {
//			taskPage.setTaskEndDate(new Date());
//			System.out.println("taskcomplete"+taskPage);
//			myTaskService.complete(taskPage);
//			j.setSuccess(true);
//			j.setMsg("完成任务【"+taskPage.getTaskname()+"】");
//		}catch(Exception e){
//			j.setMsg(e.getMessage());
//			logger.error(ExceptionUtil.getExceptionMessage(e));
//		}
//		writeJson(j);
//	}
	/**
	 * 指定执行者	包括签收和转移(指定为自己和别人)
	 */
	public void assign(){
		Json j = new Json();
		try {
			taskPage.setTaskAssignDate(new Date());
			myTaskService.assign(taskPage);
			j.setSuccess(true);
			j.setMsg("将任务【"+taskPage.getTaskname()+"】指定给了【"+myTaskService.getUserNameByUserId(taskPage.getTaskAssignee())+"】！");
		} catch (Exception e) {
			j.setMsg("指定失败！");
			logger.error(ExceptionUtil.getExceptionMessage(e));
		}
		writeJson(j);
	}
//	/**
//	 * 取得任务的候选人
//	 * @return
//	 */
//	public void candidateUsers(){
//		writeJson(myTaskService.getCandidateUsers(taskPage));
//	}
	
	public final TaskPage getTaskPage() {
		return taskPage;
	}
	public final void setTaskPage(TaskPage taskPage) {
		this.taskPage = taskPage;
	}
}
