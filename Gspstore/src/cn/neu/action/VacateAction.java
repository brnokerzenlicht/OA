package cn.neu.action;

import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import cn.neu.page.VacatePage;
import cn.neu.service.VacateServiceI;

import com.jeecg.action.BaseAction;
import com.jeecg.pageModel.Json;
import com.jeecg.util.ExceptionUtil;
import com.opensymphony.xwork2.ModelDriven;

/**   
 * @Title: Action
 * @Description: 请假审批流程
 */
@SuppressWarnings("serial")
@Action(value = "vacateAction", results = { @Result(name = "vacate", location = "/oa/vacate/vacate.jsp"),
											@Result(name = "reStart", location = "/oa/vacate/reStart.jsp"),
											@Result(name = "approve", location = "/oa/vacate/approve.jsp"),
											@Result(name = "reApprove", location = "/oa/vacate/reApprove.jsp")})
public class VacateAction extends BaseAction implements ModelDriven<VacatePage> {

	private static final Logger logger = Logger.getLogger(VacateAction.class);

	private VacatePage vacatePage = new VacatePage();
	private VacateServiceI vacateService;
	
	public VacateServiceI getVacateService() {
		return vacateService;
	}
	@Autowired
	public void setVacateService(VacateServiceI vacateService) {
		this.vacateService = vacateService;
	}
	public VacatePage getModel() {
		return vacatePage;
	}
	/**
	 * 跳转到请假信息管理页面
	 */
	public String goVacate() {
		return "vacate";
	}
	/**
	 * 跳转到请假
	 * @return
	 */
	public String goReStart() {
		vacatePage = vacateService.get(vacatePage);
		logger.info(vacatePage);
		return "reStart";
	}
	/**
	 * 跳转到部门负责人审批
	 * @return
	 */
	public String goApprove() {
		logger.info(vacatePage);
		vacatePage = vacateService.get(vacatePage);
		logger.info(vacatePage);
		return "approve";
	}
	/**
	 * 跳转到办公室审批
	 * @return
	 */
	public String goReApprove() {
		vacatePage = vacateService.get(vacatePage);
		return "reApprove";
	}
	/**
	 * 获得pageHotel数据表格
	 */
	public void datagrid() {
		// 没有指定发起人时查询自己的
		if(vacatePage.getVacatename()==null){
			vacatePage.setVacatename(getSessionInfo().getRealName());
		}
		writeJson(vacateService.datagrid(vacatePage));
	}
	/**
	 * 获得无分页的所有数据
	 */
	public void combox(){
		// 没有指定发起人时查询自己的
		if(vacatePage.getVacatename()==null){
			vacatePage.setVacatename(getSessionInfo().getRealName());
		}
		writeJson(vacateService.listAll(vacatePage));
	}
	/**
	 * 添加一条请假信息  会开始一个请假流程并完成填写申请
	 */
	public void add() {
		Json j = new Json();
		try {
			if(vacatePage.getStartDate().before(new Date())){
				throw new Exception("申请开始时间必须晚于当前时间");
			}
			if(vacatePage.getStartDate().after(vacatePage.getEndDate())){
				throw new Exception("申请开始时间必须早于结束时间");
			}
			vacatePage.setVacatename(getSessionInfo().getRealName());
			vacatePage.setVacateid(getSessionInfo().getLoginName());
			vacateService.add(vacatePage);
			j.setSuccess(true);
			j.setMsg("提交申请成功！");
		} catch (Exception e) {
			j.setMsg("提交申请失败！"+e.getMessage());
			logger.error(ExceptionUtil.getExceptionMessage(e));
		}
		writeJson(j);
	}
	/**
	 * 重新申请
	 */
	public void reStart(){
		Json j = new Json();
		try {
			logger.info("restart"+vacatePage);
			vacateService.restart(vacatePage);
			j.setSuccess(true);
			j.setMsg("重新申请成功！");
		} catch (Exception e) {
			j.setMsg("重新申请失败！"+e.getMessage());
			logger.error(ExceptionUtil.getExceptionMessage(e));
		}
		writeJson(j);
	}
	/**
	 * 部门负责人审批
	 */
	public void approve(){
		Json j = new Json();
		try {
			logger.info("approve"+vacatePage);
			vacateService.approve(vacatePage);
			j.setSuccess(true);
			j.setMsg("处理成功！");
		} catch (Exception e) {
			j.setMsg("处理失败！");
			logger.error(ExceptionUtil.getExceptionMessage(e));
		}
		writeJson(j);
	}
	/**
	 * 办公室审批审批
	 */
	public void reApprove(){
		Json j = new Json();
		try {
			vacateService.reApprove(vacatePage);
			j.setSuccess(true);
			j.setMsg("处理成功！");
		} catch (Exception e) {
			j.setMsg("处理失败！");
			logger.error(ExceptionUtil.getExceptionMessage(e));
		}
		writeJson(j);
	}
	/**
	 * 删除一条请假信息	前台复选框只能选中一个
	 * @throws Exception 
	 */
	public void delete() {
		Json j = new Json();
		try {
			logger.info("delete"+vacatePage.getIds());
			vacateService.delete(vacatePage.getIds());
			j.setSuccess(true);
			j.setMsg("删除成功！");
		} catch (Exception e) {
			if(e.getLocalizedMessage().indexOf("is still running")>0){
				j.setMsg("流程正在执行，无法删除！");
//				throw new Exception("流程正在执行，无法删除！");
			}else{
				j.setMsg("删除失败！"+e.getMessage());
			}
		}
		writeJson(j);
	}
	public final VacatePage getVacatePage() {
		return vacatePage;
	}
	public final void setVacatePage(VacatePage vacatePage) {
		this.vacatePage = vacatePage;
	}
}
