package cn.neu.entity;

/**
 * 用户信息	仅供获取用户信息时使用
 */
public class ActivitiUser {

	/**用户id*/
	private String userid;

	/**用户名字*/
	private String username;
	
	public final String getUsername() {
		return username;
	}

	public final void setUsername(String username) {
		this.username = username;
	}

	public ActivitiUser(String userid,String username) {
		this.userid = userid;
		this.username = username;
	}

	public final String getUserid() {
		return userid;
	}

	public final void setUserid(String userid) {
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "ActivitiUser [userid=" + userid + "]";
	}
}
