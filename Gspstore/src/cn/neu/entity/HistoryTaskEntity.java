package cn.neu.entity;

import org.activiti.engine.history.HistoricTaskInstance;

// 仅用于保存历史任务信息提供给前台使用
public class HistoryTaskEntity {

	/**任务的ID*/
	private java.lang.String taskid;
	/**任务的名称*/
	private java.lang.String taskname;
	/**任务的描述*/
	private java.lang.String taskDescription;
	/**任务的执行者*/
	private java.lang.String taskAssignee;
	/**任务开始时间*/
	private java.util.Date taskStartDate;
	/**任务签收时间*/
	private java.util.Date taskAssignDate;
	/**任务结束时间*/
	private java.util.Date taskEndDate;
	/**任务持续时间*/
	private long taskDurationInMillis;
	
	public HistoryTaskEntity(HistoricTaskInstance hti) {
		super();
		this.taskid = hti.getId();
		this.taskname = hti.getName();
		this.taskDescription = hti.getDescription();
		this.taskAssignDate = hti.getClaimTime();
		this.taskStartDate = hti.getStartTime();
		this.taskEndDate = hti.getEndTime();
		this.taskDurationInMillis = hti.getDurationInMillis()!=null?hti.getDurationInMillis():0;
	}
	public final java.lang.String getTaskid() {
		return taskid;
	}
	public final void setTaskid(java.lang.String taskid) {
		this.taskid = taskid;
	}
	public final java.lang.String getTaskname() {
		return taskname;
	}
	public final void setTaskname(java.lang.String taskname) {
		this.taskname = taskname;
	}
	public final java.lang.String getTaskDescription() {
		return taskDescription;
	}
	public final void setTaskDescription(java.lang.String taskDescription) {
		this.taskDescription = taskDescription;
	}
	public final java.lang.String getTaskAssignee() {
		return taskAssignee;
	}
	public final void setTaskAssignee(java.lang.String taskAssignee) {
		this.taskAssignee = taskAssignee;
	}
	public final java.util.Date getTaskStartDate() {
		return taskStartDate;
	}
	public final void setTaskStartDate(java.util.Date taskStartDate) {
		this.taskStartDate = taskStartDate;
	}
	public final java.util.Date getTaskAssignDate() {
		return taskAssignDate;
	}
	public final void setTaskAssignDate(java.util.Date taskAssignDate) {
		this.taskAssignDate = taskAssignDate;
	}
	public final java.util.Date getTaskEndDate() {
		return taskEndDate;
	}
	public final void setTaskEndDate(java.util.Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}
	@Override
	public String toString() {
		return "HistoryTaskEntity [taskid=" + taskid + ", taskname=" + taskname
				+ ", taskDescription=" + taskDescription + ", taskAssignee="
				+ taskAssignee + ", taskStartDate=" + taskStartDate
				+ ", taskAssignDate=" + taskAssignDate + ", taskEndDate="
				+ taskEndDate + ", taskDurationInMillis="
				+ taskDurationInMillis + "]";
	}
	public long getTaskDurationInMillis() {
		return taskDurationInMillis;
	}
	public void setTaskDurationInMillis(long taskDurationInMillis) {
		this.taskDurationInMillis = taskDurationInMillis;
	}
}
