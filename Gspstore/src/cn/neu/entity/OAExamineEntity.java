package cn.neu.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the oaexamine database table.
 * 
 */
@Entity
@Table(name="oaexamine")
public class OAExamineEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="examine_id", unique=true, nullable=false)
	private int examineId;

	@Column(name="exam_user", nullable=false)
	private int examUser;

	@Temporal(TemporalType.DATE)
	@Column(name="examine_tim", nullable=false)
	private Date examineTim;

	@Column(name="next_level")
	private int nextLevel;

	@Column(nullable=false)
	private String opinion;

	@Column(length=20)
	private String remark;

	private String status;

	//bi-directional many-to-one association to Oanotice
	@ManyToOne
	@JoinColumn(name="notice_id")
	private OANoticeEntity oanotice;

	public OAExamineEntity() {
	}

	public int getExamineId() {
		return this.examineId;
	}

	public void setExamineId(int examineId) {
		this.examineId = examineId;
	}

	public int getExamUser() {
		return this.examUser;
	}

	public void setExamUser(int examUser) {
		this.examUser = examUser;
	}

	public Date getExamineTim() {
		return this.examineTim;
	}

	public void setExamineTim(Date examineTim) {
		this.examineTim = examineTim;
	}

	public int getNextLevel() {
		return this.nextLevel;
	}

	public void setNextLevel(int nextLevel) {
		this.nextLevel = nextLevel;
	}

	public Object getOpinion() {
		return this.opinion;
	}

	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Object getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public final OANoticeEntity getOanotice() {
		return oanotice;
	}

	public final void setOanotice(OANoticeEntity oanotice) {
		this.oanotice = oanotice;
	}
}