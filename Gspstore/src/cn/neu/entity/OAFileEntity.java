package cn.neu.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the oafile database table.
 * 
 */
@Entity
@Table(name="oafile")
public class OAFileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="file_id", unique=true, nullable=false)
	private int fileId;

	@Column(length=20)
	private String classification;

	@Column(length=20)
	private String depart;

	@Column(name="file_path", nullable=false, length=100)
	private String filePath;

	@Column(name="file_size")
	private int fileSize;

	@Column(nullable=false, length=100)
	private String filename;

	@Column(length=20)
	private String remark;

	@Temporal(TemporalType.DATE)
	@Column(name="upload_time")
	private Date uploadTime;

	//bi-directional many-to-one association to Oanotice
	@OneToMany(mappedBy="oafile")
	private List<OANoticeEntity> oanotices;

	public OAFileEntity() {
	}

	public int getFileId() {
		return this.fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public String getClassification() {
		return this.classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getDepart() {
		return this.depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getFileSize() {
		return this.fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getUploadTime() {
		return this.uploadTime;
	}

	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}

	public final List<OANoticeEntity> getOanotices() {
		return oanotices;
	}

	public final void setOanotices(List<OANoticeEntity> oanotices) {
		this.oanotices = oanotices;
	}
}