package cn.neu.entity;

import java.io.Serializable;
import javax.persistence.*;

import cn.neu.page.NoticePage;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the oanotice database table.
 * 
 */
@Entity
@Table(name="oanotice")
public class OANoticeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer noticeId;

	private String computerIp;

	private String context;

	private String depart;

	private Date expiryTime;

	private byte isAnnouncement;

	private byte isDealed;

	private Integer priority;

	private Date releaseTime;

	private String receivedUser;

	private String releaseUser;

	private String remark;

	private String title;

	private Date typeinTime;

	private List<OAExamineEntity> oaexamines;

	private OAFileEntity oafile;

	public OANoticeEntity() {
	}

	public OANoticeEntity(NoticePage noticePage) {
		this.computerIp = noticePage.getComputerIp();
		this.context = noticePage.getContext();
		this.depart = noticePage.getDepart();
		this.expiryTime = noticePage.getExpiryTime();
		this.isAnnouncement = noticePage.getIsAnnouncement()!=null?(Byte) noticePage.getIsAnnouncement():-1;
		this.noticeId = noticePage.getNoticeId();
		this.priority = noticePage.getPriority();
		this.releaseTime = noticePage.getReleaseTime();
		this.releaseUser = noticePage.getReleaseUserId();
		this.remark = noticePage.getRemark();
		this.title = noticePage.getTitle();
		this.typeinTime = noticePage.getTypeinTime();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="notice_id", unique=true, nullable=false)
	public Integer getNoticeId() {
		return this.noticeId;
	}

	public void setNoticeId(Integer noticeId) {
		this.noticeId = noticeId;
	}

	@Column(name="computer_ip", length=20)
	public String getComputerIp() {
		return this.computerIp;
	}

	public void setComputerIp(String computerIp) {
		this.computerIp = computerIp;
	}

	@Column(name="context")
	public String getContext() {
		return this.context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	@Column(name="depart",nullable=false, length=20)
	public String getDepart() {
		return this.depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	@Column(name="expiry_time", nullable=false)
	public Date getExpiryTime() {
		return this.expiryTime;
	}

	public void setExpiryTime(Date expiryTime) {
		this.expiryTime = expiryTime;
	}

	@Column(name="is_announcement", nullable=false)
	public byte getIsAnnouncement() {
		return this.isAnnouncement;
	}

	public void setIsAnnouncement(byte isAnnouncement) {
		this.isAnnouncement = isAnnouncement;
	}

	@Column(name="is_dealed")
	public byte getIsDealed() {
		return this.isDealed;
	}

	public void setIsDealed(byte isDealed) {
		this.isDealed = isDealed;
	}

	@Column(name="priority")
	public Integer getPriority() {
		return this.priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	@Column(name="release_time", nullable=false)
	public Date getReleaseTime() {
		return this.releaseTime;
	}

	public void setReleaseTime(Date releaseTime) {
		this.releaseTime = releaseTime;
	}

	@Column(name="received_user")
	public String getReceivedUser() {
		return this.receivedUser;
	}

	public void setReceivedUser(String receivedUser) {
		this.receivedUser = receivedUser;
	}

	@Column(name="release_user", nullable=false)
	public String getReleaseUser() {
		return this.releaseUser;
	}

	public void setReleaseUser(String releaseUser) {
		this.releaseUser = releaseUser;
	}

	@Column(name="remark",length=20)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name="title",nullable=false, length=50)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="typein_time", nullable=false)
	public Date getTypeinTime() {
		return this.typeinTime;
	}

	public void setTypeinTime(Date typeinTime) {
		this.typeinTime = typeinTime;
	}

	//bi-directional many-to-one association to Oaexamine
	@OneToMany(mappedBy="oanotice")
	public final List<OAExamineEntity> getOaexamines() {
		return oaexamines;
	}

	public final void setOaexamines(List<OAExamineEntity> oaexamines) {
		this.oaexamines = oaexamines;
	}

	//bi-directional many-to-one association to Oafile
	@ManyToOne
	@JoinColumn(name="file_id")
	public final OAFileEntity getOafile() {
		return oafile;
	}

	public final void setOafile(OAFileEntity oafile) {
		this.oafile = oafile;
	}

	@Override
	public String toString() {
		return "OANoticeEntity [noticeId=" + noticeId + this.getTypeinTime() +"]";
	}
}