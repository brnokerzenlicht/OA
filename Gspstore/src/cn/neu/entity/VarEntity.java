package cn.neu.entity;

// 仅用于保存变量数据提供给前台使用
// TODO 暂未使用
public class VarEntity {

	private String id;
	private String name;
	private Object value;
	private String type;
	
	public final String getType() {
		return type;
	}
	public final void setType(String type) {
		this.type = type;
	}
	public final String getId() {
		return id;
	}
	public final void setId(String id) {
		this.id = id;
	}
	public final String getName() {
		return name;
	}
	public final void setName(String name) {
		this.name = name;
	}
	public final Object getValue() {
		return value;
	}
	public final void setValue(Object value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "VarEntity [id=" + id + ", name=" + name + ", value=" + value
				+ ", type=" + type + "]";
	}
}
