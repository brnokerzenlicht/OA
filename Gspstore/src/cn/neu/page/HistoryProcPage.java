package cn.neu.page;

import java.util.ArrayList;

import cn.neu.entity.HistoryTaskEntity;

import com.core.base.BasePage;

/*
 * @Title: Page
 * @Description: 历史流程
 */
public class HistoryProcPage extends BasePage implements java.io.Serializable {
	private static final long serialVersionUID = -8769729770390345659L;
	/** 查找类型	由我开始或者包含我的 */
	private java.lang.String procType;
	/** 流程ID */
	private java.lang.String procid;
	/** 流程名称 */
	private java.lang.String procname;
	/** 流程发起者 */
	private java.lang.String startUser;
	/** 流程开始时间 */
	private java.util.Date startDate;
	/** 流程结束时间 */
	private java.util.Date endDate;
	/** 流程开始时间开始 */
	private java.util.Date beginStartDate;
	/** 流程开始时间结束 */
	private java.util.Date endStartDate;
	/** 流程结束时间开始 */
	private java.util.Date beginEndDate;
	/** 流程结束时间结束 */
	private java.util.Date endEndDate;
	/** 流程图路径 */
	private java.lang.String diagramPath;
	/**流程包含的任务集合*/
	private java.util.List<HistoryTaskEntity> historyTasks;

	public HistoryProcPage() {
		super();
		this.historyTasks = new ArrayList<HistoryTaskEntity>();
	}
	
	public final java.util.Date getEndDate() {
		return endDate;
	}

	public final void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}

	public final java.util.Date getBeginEndDate() {
		return beginEndDate;
	}

	public final void setBeginEndDate(java.util.Date beginEndDate) {
		this.beginEndDate = beginEndDate;
	}

	public final java.util.Date getEndEndDate() {
		return endEndDate;
	}

	public final void setEndEndDate(java.util.Date endEndDate) {
		this.endEndDate = endEndDate;
	}

	public final java.lang.String getDiagramPath() {
		return diagramPath;
	}

	public final void setDiagramPath(java.lang.String diagramPath) {
		this.diagramPath = diagramPath;
	}

	public final java.lang.String getProcid() {
		return procid;
	}

	public final void setProcid(java.lang.String procid) {
		this.procid = procid;
	}

	public final java.lang.String getProcname() {
		return procname;
	}

	public final void setProcname(java.lang.String procname) {
		this.procname = procname;
	}

	public final java.util.Date getStartDate() {
		return startDate;
	}

	public final void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}

	public final java.util.Date getBeginStartDate() {
		return beginStartDate;
	}

	public final void setBeginStartDate(java.util.Date beginStartDate) {
		this.beginStartDate = beginStartDate;
	}

	public final java.util.Date getEndStartDate() {
		return endStartDate;
	}

	public final void setEndStartDate(java.util.Date endStartDate) {
		this.endStartDate = endStartDate;
	}

	@Override
	public String toString() {
		return "HistoryProcPage [procType=" + procType + ", procid=" + procid
				+ ", procname=" + procname + ", startUser=" + startUser
				+ ", startDate=" + startDate + ", endDate=" + endDate
				+ ", beginStartDate=" + beginStartDate + ", endStartDate="
				+ endStartDate + ", beginEndDate=" + beginEndDate
				+ ", endEndDate=" + endEndDate + ", diagramPath=" + diagramPath
				+ ", historyTasks=" + historyTasks + "]";
	}

	public java.util.List<HistoryTaskEntity> getHistoryTasks() {
		return historyTasks;
	}

	public void addHistoryTasks(HistoryTaskEntity historyTask) {
		this.historyTasks.add(historyTask);
	}

	public java.lang.String getStartUser() {
		return startUser;
	}

	public void setStartUser(java.lang.String startUser) {
		this.startUser = startUser;
	}

	public java.lang.String getProcType() {
		return procType;
	}

	public void setProcType(java.lang.String procType) {
		this.procType = procType;
	}
}
