package cn.neu.page;

import cn.neu.entity.OANoticeEntity;

import com.core.base.BasePage;

/**
 *@类:NoticePage
 */
@SuppressWarnings("serial")
public class NoticePage extends BasePage implements java.io.Serializable {

	/** noticeId */
	private Integer noticeId;
	/** 标题 */
	private java.lang.String title;
	/** 公告内容 */
	private java.lang.String context;
	/** 发布时间 */
	private java.util.Date releaseTime;
	/** 发布部门 */
	private java.lang.String depart;
	/** 发布人Id */
	private String releaseUserId;
	/** 发布人姓名 */
	private String releaseUserName;
	/** 录入时间 */
	private java.util.Date typeinTime;
	/** 录入ip */
	private java.lang.String computerIp;
	/** 失效时间 */
	private java.util.Date expiryTime;
	/** 优先次序 */
	private Integer priority;
	/** 类型 */
	private Byte isAnnouncement;
	/** 接收人Id 逗号分隔的字符串*/
	private String receivedUserIds;
	/** 接收人姓名 逗号分隔的字符串*/
	private String receivedUserNames;
	/** 备注 */
	private java.lang.String remark;
	
	/** 查询类型 */
	private java.lang.String queryType;
	/** 只查询未读信息 */
	private boolean isQueryUnRead;
	/** 已读标记 */
	private byte isDealed;
	
	/**录入时间开始*/
	private java.util.Date beginTypeinTime;
	/**录入时间结束*/
	private java.util.Date endTypeinTime;
	/**发布时间开始*/
	private java.util.Date beginReleaseTime;
	/**发布时间结束*/
	private java.util.Date endReleaseTime;
	/**失效时间开始*/
	private java.util.Date beginExpiryTime;
	/**失效时间结束*/
	private java.util.Date endExpiryTime;
	
	public NoticePage(){
	}
	
	public NoticePage(OANoticeEntity tb) {
		this.noticeId = tb.getNoticeId();
		this.computerIp = tb.getComputerIp();
		this.context = tb.getContext();
		this.depart = tb.getDepart();
		this.expiryTime = tb.getExpiryTime();
		this.isAnnouncement = tb.getIsAnnouncement();
		this.priority = tb.getPriority();
		this.receivedUserIds = null;
		this.receivedUserNames = null;
		this.releaseTime = tb.getReleaseTime();
		this.releaseUserId = tb.getReleaseUser();
		this.remark = tb.getRemark();
		this.title = tb.getTitle();
		this.typeinTime = tb.getTypeinTime();
		this.isDealed = tb.getIsDealed();
	}
	
	public final java.lang.String getQueryType() {
		return queryType;
	}

	public final void setQueryType(java.lang.String queryType) {
		this.queryType = queryType;
	}

	public final boolean isQueryUnRead() {
		return isQueryUnRead;
	}

	public final void setQueryUnRead(boolean isQueryUnRead) {
		this.isQueryUnRead = isQueryUnRead;
	}

	public final Integer getNoticeId() {
		return noticeId;
	}
	public final void setNoticeId(Integer noticeId) {
		this.noticeId = noticeId;
	}
	public final java.lang.String getTitle() {
		return title;
	}
	public final void setTitle(java.lang.String title) {
		this.title = title;
	}
	public final java.lang.String getContext() {
		return context;
	}
	public final void setContext(java.lang.String context) {
		this.context = context;
	}
	public final java.util.Date getReleaseTime() {
		return releaseTime;
	}
	public final void setReleaseTime(java.util.Date releaseTime) {
		this.releaseTime = releaseTime;
	}
	public final java.lang.String getDepart() {
		return depart;
	}
	public final void setDepart(java.lang.String depart) {
		this.depart = depart;
	}
	public final String getReleaseUserId() {
		return releaseUserId;
	}
	public final void setReleaseUserId(String releaseUserId) {
		this.releaseUserId = releaseUserId;
	}
	public final String getReleaseUserName() {
		return releaseUserName;
	}
	public final void setReleaseUserName(String releaseUserName) {
		this.releaseUserName = releaseUserName;
	}
	public final java.util.Date getTypeinTime() {
		return typeinTime;
	}
	public final void setTypeinTime(java.util.Date typeinTime) {
		this.typeinTime = typeinTime;
	}
	public final java.lang.String getComputerIp() {
		return computerIp;
	}
	public final void setComputerIp(java.lang.String computerIp) {
		this.computerIp = computerIp;
	}
	public final java.util.Date getExpiryTime() {
		return expiryTime;
	}
	public final void setExpiryTime(java.util.Date expiryTime) {
		this.expiryTime = expiryTime;
	}
	public final Integer getPriority() {
		return priority;
	}
	public final void setPriority(Integer priority) {
		this.priority = priority;
	}
	public final Byte getIsAnnouncement() {
		return isAnnouncement;
	}
	public final void setIsAnnouncement(Byte isAnnouncement) {
		this.isAnnouncement = isAnnouncement;
	}
	public final String getReceivedUserIds() {
		return receivedUserIds;
	}
	public final void setReceivedUserIds(String receivedUserIds) {
		this.receivedUserIds = receivedUserIds;
	}
	public final void addReceivedUserIds(String receivedUserId) {
		if(this.receivedUserIds==null){
			this.receivedUserIds = "" + receivedUserId;
		}else{
			this.receivedUserIds += "," +receivedUserId;
		}
	}
	public final void addReceivedUserNames(String receivedUserName) {
		if(this.receivedUserNames==null){
			this.receivedUserNames = "" + receivedUserName;
		}else{
			this.receivedUserNames += "," +receivedUserName;
		}
	}
	public final String getReceivedUserNames() {
		return receivedUserNames;
	}
	public final void setReceivedUserNames(String receivedUserNames) {
		this.receivedUserNames = receivedUserNames;
	}
	public final java.lang.String getRemark() {
		return remark;
	}
	public final void setRemark(java.lang.String remark) {
		this.remark = remark;
	}
	
	public final java.util.Date getBeginTypeinTime() {
		return beginTypeinTime;
	}

	public final void setBeginTypeinTime(java.util.Date beginTypeinTime) {
		this.beginTypeinTime = beginTypeinTime;
	}

	public final java.util.Date getEndTypeinTime() {
		return endTypeinTime;
	}

	public final void setEndTypeinTime(java.util.Date endTypeinTime) {
		this.endTypeinTime = endTypeinTime;
	}

	public final java.util.Date getBeginReleaseTime() {
		return beginReleaseTime;
	}

	public final void setBeginReleaseTime(java.util.Date beginReleaseTime) {
		this.beginReleaseTime = beginReleaseTime;
	}

	public final java.util.Date getEndReleaseTime() {
		return endReleaseTime;
	}

	public final void setEndReleaseTime(java.util.Date endReleaseTime) {
		this.endReleaseTime = endReleaseTime;
	}

	public final java.util.Date getBeginExpiryTime() {
		return beginExpiryTime;
	}

	public final void setBeginExpiryTime(java.util.Date beginExpiryTime) {
		this.beginExpiryTime = beginExpiryTime;
	}

	public final java.util.Date getEndExpiryTime() {
		return endExpiryTime;
	}

	public final void setEndExpiryTime(java.util.Date endExpiryTime) {
		this.endExpiryTime = endExpiryTime;
	}

	public final byte getIsDealed() {
		return isDealed;
	}

	public final void setIsDealed(byte isDealed) {
		this.isDealed = isDealed;
	}

	@Override
	public String toString() {
		return "NoticePage [noticeId=" + noticeId + ", title=" + title
				+ ", context=" + context + ", releaseTime=" + releaseTime
				+ ", depart=" + depart + ", releaseUserId=" + releaseUserId
				+ ", releaseUserName=" + releaseUserName + ", typeinTime="
				+ typeinTime + ", computerIp=" + computerIp + ", expiryTime="
				+ expiryTime + ", priority=" + priority + ", isAnnouncement="
				+ isAnnouncement + ", receivedUserIds=" + receivedUserIds
				+ ", receivedUserNames=" + receivedUserNames + ", remark="
				+ remark + ", queryType=" + queryType + ", isQueryUnRead="
				+ isQueryUnRead + ", isDealed=" + isDealed
				+ ", beginTypeinTime=" + beginTypeinTime + ", endTypeinTime="
				+ endTypeinTime + ", beginReleaseTime=" + beginReleaseTime
				+ ", endReleaseTime=" + endReleaseTime + ", beginExpiryTime="
				+ beginExpiryTime + ", endExpiryTime=" + endExpiryTime + "]";
	}
}