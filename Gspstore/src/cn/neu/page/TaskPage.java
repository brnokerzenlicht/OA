package cn.neu.page;

import java.util.ArrayList;

import cn.neu.entity.ActivitiUser;

import com.core.base.BasePage;

/*
 * @Title: Page
 * @Description: 我的任务
 */
public class TaskPage extends BasePage implements java.io.Serializable {
	private static final long serialVersionUID = -8769729770390345659L;
	/** 任务的ID */
	private java.lang.String taskid;
	/** 任务的名称 */
	private java.lang.String taskname;
	/** 任务的描述 */
	private java.lang.String taskDescription;
	/** 任务的执行者 */
	private java.lang.String taskAssignee;
	/** 任务的执行者名字 */
	private java.lang.String taskAssigneeName;
	/** 流程ID */
	private java.lang.String procid;
	/** 流程名称 */
	private java.lang.String procname;
	/** 流程发起者 */
	private java.lang.String startUser;
	/** 流程开始时间 */
	private java.util.Date startDate;
	/** 任务开始时间 */
	private java.util.Date taskStartDate;
	/** 任务签收时间 */
	private java.util.Date taskAssignDate;
	/** 任务结束时间 */
	private java.util.Date taskEndDate;
//	/** 已经有的流程变量 */
//	private java.util.List<VarEntity> hadVars;
//	/** 完成任务需要的流程变量 */
//	private java.util.List<VarEntity> needVars;
	/** 任务的formkey 内容为完成该任务需要跳转到的action */
	private java.lang.String formKey;
	/** 任务候选人列表 */
	private java.util.List<ActivitiUser> candidateUsers;
	/** 流程开始时间开始 */
	private java.util.Date beginStartDate;
	/** 流程开始时间结束 */
	private java.util.Date endStartDate;
	/** 任务开始时间开始 */
	private java.util.Date beginTaskStartDate;
	/** 任务开始时间结束 */
	private java.util.Date endTaskStartDate;
	/** 任务签收时间开始 */
	private java.util.Date beginTaskAssignDate;
	/** 任务签收时间结束 */
	private java.util.Date endTaskAssignDate;
	/** 任务结束时间开始 */
	private java.util.Date beginTaskEndDate;
	/** 任务结束时间结束 */
	private java.util.Date endTaskEndDate;

	public TaskPage() {
		super();
//		needVars = new ArrayList<VarEntity>();
//		hadVars = new ArrayList<VarEntity>();
		candidateUsers = new ArrayList<ActivitiUser>();
	}

	public final java.lang.String getProcid() {
		return procid;
	}

	public final void setProcid(java.lang.String procid) {
		this.procid = procid;
	}

	public final java.lang.String getProcname() {
		return procname;
	}

	public final void setProcname(java.lang.String procname) {
		this.procname = procname;
	}

	public final java.lang.String getFormKey() {
		return formKey;
	}

	public final void setFormKey(java.lang.String formKey) {
		this.formKey = formKey;
	}

	public final java.util.Date getTaskAssignDate() {
		return taskAssignDate;
	}

	public final void setTaskAssignDate(java.util.Date taskAssignDate) {
		this.taskAssignDate = taskAssignDate;
	}

	public final java.util.Date getBeginTaskAssignDate() {
		return beginTaskAssignDate;
	}

	public final void setBeginTaskAssignDate(java.util.Date beginTaskAssignDate) {
		this.beginTaskAssignDate = beginTaskAssignDate;
	}

	public final java.util.Date getEndTaskAssignDate() {
		return endTaskAssignDate;
	}

	public final void setEndTaskAssignDate(java.util.Date endTaskAssignDate) {
		this.endTaskAssignDate = endTaskAssignDate;
	}

	public final java.lang.String getTaskAssignee() {
		return taskAssignee;
	}

	public final void setTaskAssignee(java.lang.String taskAssignee) {
		this.taskAssignee = taskAssignee;
	}

	public final java.lang.String getTaskid() {
		return taskid;
	}

	public final void setTaskid(java.lang.String taskid) {
		this.taskid = taskid;
	}

	public final java.lang.String getTaskname() {
		return taskname;
	}

	public final void setTaskname(java.lang.String taskname) {
		this.taskname = taskname;
	}

	public final java.lang.String getTaskDescription() {
		return taskDescription;
	}

	public final void setTaskDescription(java.lang.String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public final java.util.Date getStartDate() {
		return startDate;
	}

	public final void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}

	public final java.util.Date getTaskStartDate() {
		return taskStartDate;
	}

	public final void setTaskStartDate(java.util.Date taskStartDate) {
		this.taskStartDate = taskStartDate;
	}

	public final java.util.Date getTaskEndDate() {
		return taskEndDate;
	}

	public final void setTaskEndDate(java.util.Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}

	public final java.util.Date getBeginStartDate() {
		return beginStartDate;
	}

	public final void setBeginStartDate(java.util.Date beginStartDate) {
		this.beginStartDate = beginStartDate;
	}

	public final java.util.Date getEndStartDate() {
		return endStartDate;
	}

	public final void setEndStartDate(java.util.Date endStartDate) {
		this.endStartDate = endStartDate;
	}

	public final java.util.Date getBeginTaskStartDate() {
		return beginTaskStartDate;
	}

	public final void setBeginTaskStartDate(java.util.Date beginTaskStartDate) {
		this.beginTaskStartDate = beginTaskStartDate;
	}

	public final java.util.Date getEndTaskStartDate() {
		return endTaskStartDate;
	}

	public final void setEndTaskStartDate(java.util.Date endTaskStartDate) {
		this.endTaskStartDate = endTaskStartDate;
	}

	public final java.util.Date getBeginTaskEndDate() {
		return beginTaskEndDate;
	}

	public final void setBeginTaskEndDate(java.util.Date beginTaskEndDate) {
		this.beginTaskEndDate = beginTaskEndDate;
	}

	public final java.util.Date getEndTaskEndDate() {
		return endTaskEndDate;
	}

	public final void setEndTaskEndDate(java.util.Date endTaskEndDate) {
		this.endTaskEndDate = endTaskEndDate;
	}

//	public final java.util.List<VarEntity> getHadVars() {
//		return hadVars;
//	}
//
//	public final void addHadVars(VarEntity var) {
//		this.hadVars.add(var);
//	}
//
//	public final java.util.List<VarEntity> getNeedVars() {
//		return needVars;
//	}
//
//	public final void addNeedVars(VarEntity var) {
//		this.needVars.add(var);
//	}

	public final void addCandidateUsers(ActivitiUser user) {
		this.candidateUsers.add(user);
	}

	public final java.util.List<ActivitiUser> getCandidateUsers() {
		return candidateUsers;
	}

	public final java.lang.String getTaskAssigneeName() {
		return taskAssigneeName;
	}

	public final void setTaskAssigneeName(java.lang.String taskAssigneeName) {
		this.taskAssigneeName = taskAssigneeName;
	}

	@Override
	public String toString() {
		return "TaskPage [taskid=" + taskid + ", taskname=" + taskname
				+ ", taskDescription=" + taskDescription + ", taskAssignee="
				+ taskAssignee + ", procid=" + procid + ", procname="
				+ procname + ", startDate=" + startDate + ", taskStartDate="
				+ taskStartDate + ", taskAssignDate=" + taskAssignDate
				+ ", taskEndDate=" + taskEndDate + ", formKey=" + formKey
				+ ", candidateUsers=" + candidateUsers + ", beginStartDate="
				+ beginStartDate + ", endStartDate=" + endStartDate
				+ ", beginTaskStartDate=" + beginTaskStartDate
				+ ", endTaskStartDate=" + endTaskStartDate
				+ ", beginTaskAssignDate=" + beginTaskAssignDate
				+ ", endTaskAssignDate=" + endTaskAssignDate
				+ ", beginTaskEndDate=" + beginTaskEndDate
				+ ", endTaskEndDate=" + endTaskEndDate + "]";
	}

	public java.lang.String getStartUser() {
		return startUser;
	}

	public void setStartUser(java.lang.String startUser) {
		this.startUser = startUser;
	}
}
