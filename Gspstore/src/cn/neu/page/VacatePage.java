package cn.neu.page;

import com.core.base.BasePage;

/*
 * @Title: Page
 * @Description: 请假信息
 */
public class VacatePage extends BasePage implements java.io.Serializable {
	private static final long serialVersionUID = -8769729770390345659L;
	/** 请假ID 对应流程实例的ID */
	private java.lang.String procid;
	/** 任务ID 用于完成任务 */
	private java.lang.String taskid;
	/** 执行者 */
	private java.lang.String taskAssignee;
	/** 姓名 */
	private java.lang.String vacatename;
	/** 请假者id */
	private java.lang.String vacateid;
	/** 开始时间 */
	private java.util.Date startDate;
	/** 结束时间 */
	private java.util.Date endDate;
	/** 请假原因 */
	private java.lang.String reason;
	/** 部门负责人审批 流程图利用的是true和false可以换成String，需要修改流程图 */
	private boolean approve;
	/** 部门负责人审批意见 */
	private String approveSuggestion;
	/** 办公室审批 */
	private boolean reApprove;
	/** 办公室审批意见 */
	private String reApproveSuggestion;
	/** 开始时间开始 */
	private java.util.Date beginStartDate;
	/** 开始时间结束 */
	private java.util.Date endStartDate;
	/** 结束时间开始 */
	private java.util.Date beginEndDate;
	/** 结束时间结束 */
	private java.util.Date endEndDate;

	public final java.lang.String getTaskid() {
		return taskid;
	}

	public final void setTaskid(java.lang.String taskid) {
		this.taskid = taskid;
	}

	public final java.lang.String getTaskAssignee() {
		return taskAssignee;
	}

	public final void setTaskAssignee(java.lang.String taskAssignee) {
		this.taskAssignee = taskAssignee;
	}

	public final String getApproveSuggestion() {
		return approveSuggestion;
	}

	public final void setApproveSuggestion(String approveSuggestion) {
		this.approveSuggestion = approveSuggestion;
	}

	public final String getReApproveSuggestion() {
		return reApproveSuggestion;
	}

	public final void setReApproveSuggestion(String reApproveSuggestion) {
		this.reApproveSuggestion = reApproveSuggestion;
	}

	public final boolean getApprove() {
		return approve;
	}

	public final void setApprove(boolean approve) {
		this.approve = approve;
	}

	public final boolean getReApprove() {
		return reApprove;
	}

	public final void setReApprove(boolean reApprove) {
		this.reApprove = reApprove;
	}

	public final java.lang.String getProcid() {
		return procid;
	}

	public final void setProcid(java.lang.String procid) {
		this.procid = procid;
	}

	public final java.lang.String getVacatename() {
		return vacatename;
	}

	public final void setVacatename(java.lang.String vacatename) {
		this.vacatename = vacatename;
	}

	public final java.util.Date getStartDate() {
		return startDate;
	}

	public final void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}

	public final java.util.Date getEndDate() {
		return endDate;
	}

	public final void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}

	public final java.lang.String getReason() {
		return reason;
	}

	public final void setReason(java.lang.String reason) {
		this.reason = reason;
	}

	public final java.util.Date getBeginStartDate() {
		return beginStartDate;
	}

	public final void setBeginStartDate(java.util.Date beginStartDate) {
		this.beginStartDate = beginStartDate;
	}

	public final java.util.Date getEndStartDate() {
		return endStartDate;
	}

	public final void setEndStartDate(java.util.Date endStartDate) {
		this.endStartDate = endStartDate;
	}

	public final java.util.Date getBeginEndDate() {
		return beginEndDate;
	}

	public final void setBeginEndDate(java.util.Date beginEndDate) {
		this.beginEndDate = beginEndDate;
	}

	public final java.util.Date getEndEndDate() {
		return endEndDate;
	}

	public final void setEndEndDate(java.util.Date endEndDate) {
		this.endEndDate = endEndDate;
	}

	@Override
	public String toString() {
		return "VacatePage [vacateid=" + procid + ", taskid=" + taskid
				+ ", taskAssignee=" + taskAssignee + ", vacatename="
				+ vacatename + ", startDate=" + startDate + ", endDate="
				+ endDate + ", reason=" + reason + ", approve=" + approve
				+ ", approveSuggestion=" + approveSuggestion + ", reApprove="
				+ reApprove + ", reApproveSuggestion=" + reApproveSuggestion
				+ ", beginStartDate=" + beginStartDate + ", endStartDate="
				+ endStartDate + ", beginEndDate=" + beginEndDate
				+ ", endEndDate=" + endEndDate + "]";
	}

	public java.lang.String getVacateid() {
		return vacateid;
	}

	public void setVacateid(java.lang.String vacateid) {
		this.vacateid = vacateid;
	}
}
