package cn.neu.service;

import java.util.List;

import cn.neu.page.HistoryProcPage;

import com.jeecg.pageModel.DataGrid;
import com.jeecg.service.BaseServiceI;

/**   
 * @Title: Service
 * @Description: 历史流程信息
 */
public interface HistoryProcServiceI  extends BaseServiceI {

	/**
	 * 获得数据表格
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(HistoryProcPage historyProcPage,String userId);
	
	/**
	 * 获得
	 * @param procid
	 * @return
	 */
	public HistoryProcPage get(java.lang.String procid);
	
	/**
	 * 删除
	 * @param procid
	 * @return
	 */
	public void delete(java.lang.String procid);
	
	/**
	 * 获取所有数据
	 */
	public List<HistoryProcPage> listAll(HistoryProcPage historyProcPage,String userId);
	
}
