package cn.neu.service;

import java.util.List;

import cn.neu.page.TaskPage;

import com.jeecg.pageModel.DataGrid;
import com.jeecg.service.BaseServiceI;

/**   
 * @Title: Service
 * @Description: 我的任务信息
 */
public interface MyTaskServiceI  extends BaseServiceI {

	/**
	 * 获得数据表格
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(TaskPage taskPage,String userid);

	/**
	 * 指定执行者	包括签收和转移(指定为自己和别人)
	 * @param accounterPage
	 */
	public void assign(TaskPage taskPage);

	/**
	 * 获得
	 * @param taskid
	 * @return
	 */
	public TaskPage get(java.lang.String taskid);
	
	/**
	 * 获取所有数据
	 */
	public List<TaskPage> listAll(TaskPage taskPage,String userid);

	/**
	 * 获取任务的候选人列表
	 * @param taskPage
	 * @return
	 */
	public void getCandidateUsers(TaskPage taskPage);

	/**
	 * 根据执行者id获取执行者名字
	 */
	public String getUserNameByUserId(String taskAssignee);
	
}
