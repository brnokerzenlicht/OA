package cn.neu.service;

import java.util.List;

import cn.neu.page.NoticePage;

import com.jeecg.pageModel.DataGrid;
import com.jeecg.service.BaseServiceI;

/**   
 * @Title: Service
 * @Description: 公告通知
 */
public interface NoticeServiceI extends BaseServiceI {

	/**
	 * 获得数据表格
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(NoticePage noticePage);

	/**
	 * 获得数据表格
	 * @param userId 
	 * @param bug
	 * @return
	 * @throws Exception 
	 */
	public DataGrid datagridUnGroup(NoticePage noticePage, String userId) throws Exception;

	/**
	 * 添加
	 * @param noticePage
	 * @throws Exception 
	 */
	public void add(NoticePage noticePage) throws Exception;

	/**
	 * 修改
	 * @param noticePage
	 */
	public void update(NoticePage noticePage) throws Exception;

	/**
	 * 删除
	 * @param ids
	 */
	public void delete(String ids);

	/**
	 * 获得
	 * @param noticePage
	 * @return
	 * @throws Exception 
	 */
	public NoticePage get(NoticePage noticePage) throws Exception;
	
	public NoticePage get(String title) throws Exception;

	/**
	 * 获得
	 * @param title
	 * @return
	 * @throws Exception 
	 */
	public NoticePage getByNoticeId(Integer noticeId) throws Exception;

	/**
	 * 获得
	 * @param title
	 * @return
	 * @throws Exception 
	 */
	public List<NoticePage> getByUserId(String userId);
	
	/**
	 * 获取所有数据
	 */
	public List<NoticePage> listAll(NoticePage noticePage);
	
	/**
	 * 获取所有没有发布的数据
	 */
	public List<NoticePage> listAllUnReleased();


}
