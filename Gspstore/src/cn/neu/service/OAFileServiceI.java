package cn.neu.service;

import cn.neu.entity.OAFileEntity;

import com.jeecg.service.BaseServiceI;

/**   
 * @Title: Service
 * @Description: OAFile
 * @version V1.0   
 */
public interface OAFileServiceI extends BaseServiceI {

	/**
	 * 添加
	 */
	public void add(OAFileEntity oafile);
	
	/**
	 * 更新
	 */
	public void update(OAFileEntity oafile) throws Exception;
	
	/**
	 * 删除
	 * @param fileId oafile文件id
	 */
	public void delete(int fileId);
	
	/**
	 * 查找
	 */
	public OAFileEntity getOAFileById(int fileId);
}
