package cn.neu.service;

import java.util.List;

import cn.neu.page.VacatePage;

import com.jeecg.pageModel.DataGrid;
import com.jeecg.service.BaseServiceI;

/**   
 * @Title: Service
 * @Description: 请假信息
 */
public interface VacateServiceI  extends BaseServiceI {

	/**
	 * 获得数据表格
	 * @param bug
	 * @return
	 */
	public DataGrid datagrid(VacatePage vacatePage);

	/**
	 * 添加
	 * @param vacatePage
	 */
	public void add(VacatePage vacatePage);
	/**
	 * 部门负责人审批
	 * @param vacatePage
	 * @throws Exception 
	 */
	public void approve(VacatePage vacatePage);
	/**
	 * 办公室审批
	 * @param vacatePage
	 * @throws Exception 
	 */
	public void reApprove(VacatePage vacatePage);

	/**
	 * 删除
	 * @param ids
	 */
	public void delete(String ids);

	/**
	 * 获得
	 * @param vacateid
	 * @return
	 */
	public VacatePage get(VacatePage vacatePage);
	
	/**
	 * 获取所有数据
	 */
	public List<VacatePage> listAll(VacatePage vacatePage);
	/**
	 * 重新申请
	 * @param vacatePage
	 * @throws Exception 
	 */
	public void restart(VacatePage vacatePage);

	
}
