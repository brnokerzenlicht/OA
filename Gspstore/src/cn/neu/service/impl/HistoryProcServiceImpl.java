package cn.neu.service.impl;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.neu.entity.HistoryTaskEntity;
import cn.neu.page.HistoryProcPage;
import cn.neu.service.HistoryProcServiceI;

import com.jeecg.pageModel.DataGrid;
import com.jeecg.service.impl.BaseServiceImpl;

/**   
 * @Title: ServiceImpl
 * @Description: 历史流程信息
 */
@Service("historyProcService")
public class HistoryProcServiceImpl extends BaseServiceImpl implements HistoryProcServiceI {

	private static final Logger logger = Logger.getLogger(HistoryProcServiceImpl.class);
	@Autowired
	private ProcessEngine processEngine;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private IdentityService identityService;
	
	private static String webRootPath = getWebRootPath();
	/**
	 * 获取表格数据
	 */
	@Transactional(propagation = Propagation.SUPPORTS)
	public DataGrid datagrid(HistoryProcPage historyProcPage,String userId) {
		DataGrid j = new DataGrid();
		j.setRows(find(historyProcPage,userId));
		j.setTotal(total(historyProcPage,userId));
		return j;
	}

	/**
	 * 有分页的历史流程信息
	 * @param historyProcPage
	 * @return
	 */
	private List<HistoryProcPage> find(HistoryProcPage historyProcPage,String userId) {
		HistoricProcessInstanceQuery historyProcQuery = historyService.createHistoricProcessInstanceQuery();
		historyProcQuery = addWhere(historyProcPage,historyProcQuery,userId);
		List<HistoricProcessInstance> list = historyProcQuery.listPage((historyProcPage.getPage()-1)*historyProcPage.getRows(), historyProcPage.getRows());
		List<HistoryProcPage> result = handle(list);
		return result;
	}

	/**
	 * 根据历史流程信息生成HistoryProcPage
	 * @param list
	 * @return
	 */
	private List<HistoryProcPage> handle(List<HistoricProcessInstance> list) {
		List<HistoryProcPage> result = new ArrayList<HistoryProcPage>();
		HistoryProcPage temp;
		List<HistoricTaskInstance> htis;
		HistoryTaskEntity historyTaskEntity;
		ProcessDefinition procDef;
		InputStream is;
		for (HistoricProcessInstance hpi : list) {
			temp = new HistoryProcPage();
			temp.setProcid(hpi.getId());
			temp.setStartDate(hpi.getStartTime());
			temp.setEndDate(hpi.getEndTime());
			temp.setStartUser(hpi.getStartUserId());
			// 根据流程id查询流程名称
			procDef = repositoryService.createProcessDefinitionQuery().processDefinitionId(hpi.getProcessDefinitionId()).singleResult();
			temp.setProcname(procDef.getName());
			if(list.size() == 1){
				// 查询结果为1时，是查询详细信息，需要查询以下内容
				// 根据流程id生成流程图
				is = generateDiagram(hpi.getId(), processEngine);
				final String fileName = "/oa/historyProc/"+System.nanoTime()+".png";
				try {
					Files.copy(is, Paths.get(webRootPath+fileName));
					temp.setDiagramPath(fileName);
					new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								System.err.println("开始执行"+System.currentTimeMillis());
								// 5秒钟后删除
								Thread.sleep(5000);
								System.err.println("开始执行"+System.currentTimeMillis());
								Files.deleteIfExists(Paths.get(webRootPath+fileName));
								System.err.println("结束执行"+System.currentTimeMillis());
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}).start();
				}catch(Exception e){
					e.printStackTrace();
					logger.info("生成流程图错误！"+e.getMessage());
				}
				// 根据流程id查询历史任务
				htis = historyService.createHistoricTaskInstanceQuery().processInstanceId(hpi.getId()).list();
				// 历史任务按照任务开始时间升序进行排序
				Collections.sort(htis, new Comparator<HistoricTaskInstance>() {
					@Override
					public int compare(HistoricTaskInstance o1,
							HistoricTaskInstance o2) {
						return o1.getStartTime().before(o2.getStartTime())?-1:1;
					}
				});
				for (HistoricTaskInstance historicTaskInstance : htis) {
					historyTaskEntity = new HistoryTaskEntity(historicTaskInstance);
					// 指定执行者名字
					historyTaskEntity.setTaskAssignee(getUserNameByUserId(historicTaskInstance.getAssignee()));
					temp.addHistoryTasks(historyTaskEntity);
				}
			}
			result.add(temp);
		}
		return result;
	}


	private InputStream generateDiagram(String id, ProcessEngine processEngine) {
	    //方法中用到的参数是流程实例ID，其实TaskId也可以转为这个。调用taskService查询即可。
	    Command<InputStream> cmd = new cn.neu.util.ProcessInstanceDiagramCmd(id, processEngine);
	    return processEngine.getManagementService().executeCommand(cmd);
	}

	/**
	 * 添加historyProcPage中的查询条件
	 * @param historyProcPage
	 * @param historyProcQuery
	 * @return
	 */
	private HistoricProcessInstanceQuery addWhere(HistoryProcPage historyProcPage,
			HistoricProcessInstanceQuery historyProcQuery,String userId) {
		if(historyProcPage.getProcType().equals("started")){
			historyProcQuery = historyProcQuery.startedBy(getUserNameByUserId(userId));
		}else if(historyProcPage.getProcType().equals("involved")){
			historyProcQuery = historyProcQuery.involvedUser(userId);
		}
		logger.info("addWhere:"+historyProcPage);
		if(historyProcPage.getBeginStartDate()!=null){
			historyProcQuery = historyProcQuery.startedAfter(historyProcPage.getBeginStartDate());
		}
		if(historyProcPage.getEndStartDate()!=null){
			historyProcQuery = historyProcQuery.startedBefore(historyProcPage.getEndStartDate());
		}
		if(historyProcPage.getBeginEndDate()!=null){
			historyProcQuery = historyProcQuery.finishedAfter(historyProcPage.getBeginEndDate());
		}
		if(historyProcPage.getEndEndDate()!=null){
			historyProcQuery = historyProcQuery.finishedBefore(historyProcPage.getEndEndDate());
		}
		if(historyProcPage.getProcid()!=null&&historyProcPage.getProcid().trim().length()>0){
			historyProcQuery = historyProcQuery.processInstanceId(historyProcPage.getProcid());
		}
		if(historyProcPage.getProcname()!=null&&historyProcPage.getProcname().trim().length()>0){
			historyProcQuery = historyProcQuery.processInstanceName(historyProcPage.getProcname());
		}
		return historyProcQuery.orderByProcessInstanceStartTime().desc();//.processInstanceNameLike("taskProcess");
	}

	/**
	 * 历史流程信息的条数
	 * @param historyProcPage
	 * @return
	 */
	private Long total(HistoryProcPage historyProcPage,String userId) {
		HistoricProcessInstanceQuery historyProcQuery = historyService.createHistoricProcessInstanceQuery();
		historyProcQuery = addWhere(historyProcPage,historyProcQuery,userId);
		return historyProcQuery.count();
	}


	/**
	 * 无分页历史流程信息
	 */
	@Override
	public List<HistoryProcPage> listAll(HistoryProcPage historyProcPage,String userId) {
		HistoricProcessInstanceQuery historyProcQuery = historyService.createHistoricProcessInstanceQuery();
		historyProcQuery = addWhere(historyProcPage,historyProcQuery,userId);
		List<HistoricProcessInstance> list = historyProcQuery.list();
		List<HistoryProcPage> result = handle(list);
		return result;
	}

	@Override
	public void delete(String ids) {
		for (String id : ids.split(",")) {
			historyService.deleteHistoricProcessInstance(id);
		}
	}

	/**
	 * 取得一条历史流程信息
	 * @param procid	
	 */
	public HistoryProcPage get(java.lang.String procid) {
		HistoricProcessInstanceQuery historyProcQuery = historyService.createHistoricProcessInstanceQuery().processInstanceId(procid);
		List<HistoryProcPage> result = handle(historyProcQuery.list());
		logger.info("procid"+result);
		return result.size()>0 ? result.get(0):null;
	}

	// 获取WebRoot目录
	private static String getWebRootPath() {
		URL urlpath = HistoryProcServiceImpl.class.getResource("");
		String path = urlpath.toString();
		if (path.startsWith("file")) {
			path = path.substring(6);
		}
		if (path.indexOf("WEB-INF") > 0) {
			path = path.substring(0, path.indexOf("WEB-INF") - 1);
		}
		path.replace("/", File.separator);
		return path;
	}
	
	/**
	 * 根据执行者id获取执行者名字
	 */
	private String getUserNameByUserId(String userId){
		if(userId!=null)
			return identityService.createUserQuery().userId(userId).list().get(0).getFirstName();
		else
			return "";
	}
}
