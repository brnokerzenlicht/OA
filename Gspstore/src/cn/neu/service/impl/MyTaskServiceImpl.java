package cn.neu.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormData;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.neu.entity.ActivitiUser;
import cn.neu.page.TaskPage;
import cn.neu.service.MyTaskServiceI;

import com.jeecg.pageModel.DataGrid;
import com.jeecg.service.impl.BaseServiceImpl;

/**   
 * @Title: ServiceImpl
 * @Description: 我的任务信息
 */
@Service("myTaskService")
public class MyTaskServiceImpl extends BaseServiceImpl implements MyTaskServiceI {

	@Autowired
	private FormService formService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private IdentityService identityService;
	
	
	/**
	 * 获取表格数据
	 */
	@Transactional(propagation = Propagation.SUPPORTS)
	public DataGrid datagrid(TaskPage taskPage,String userid) {
		autoCandidate();
		DataGrid j = new DataGrid();
		j.setRows(find(taskPage,userid));
		j.setTotal(total(taskPage,userid));
		return j;
	}

	/**
	 * 将未指定任务候选人的任务指派给所在流程的发起者
	 */
	private void autoCandidate() {
		String procId;
		String userId;
		HistoricProcessInstance hpi;
		for (Task task : taskService.createTaskQuery().list()) {
			// 没有指定任务候选人
			if(taskService.getIdentityLinksForTask(task.getId()).size()==0){
				procId = task.getProcessInstanceId();
				hpi = historyService.createHistoricProcessInstanceQuery().processInstanceId(procId).list().get(0);
				userId = getUserIdByUserName(hpi.getStartUserId());
				taskService.setAssignee(task.getId(), userId);
			}
		}
	}

	/**
	 * 有分页的任务信息
	 * @param taskPage
	 * @return
	 */
	private List<TaskPage> find(TaskPage taskPage,String userid) {
		TaskQuery taskQuery = taskService.createTaskQuery();
		taskQuery = addWhere(taskPage, taskQuery,userid);
		List<Task> list = taskQuery.listPage((taskPage.getPage()-1)*taskPage.getRows(), taskPage.getRows());
		List<TaskPage> result = handle(list);
		return result;
	}

	/**
	 * 根据任务信息生成TaskPage
	 * @param list
	 * @return
	 */
	private List<TaskPage> handle(List<Task> list) {
		List<TaskPage> result = new ArrayList<TaskPage>();
		HistoricProcessInstance hpi;
		String procId;
		FormData formData;
		TaskPage temp;
//		Map<String,Object> vars;
//		VarEntity tempVar;
		ProcessDefinition procDef;
//		List<HistoricTaskInstance> historyTasks;
//		List<FormProperty> tempFps;
//		String tempTaskId;
//		Map<String,String> tempFpsMap;
		for (Task task : list) {
			temp = new TaskPage();
			temp.setTaskid(task.getId());
			temp.setTaskname(task.getName());
			temp.setTaskStartDate(task.getCreateTime());
			temp.setTaskDescription(task.getDescription());
			temp.setTaskAssignee(task.getAssignee());
			temp.setTaskAssigneeName(getUserNameByUserId(task.getAssignee()));
			// 任务签收时间	只有完成的任务才能查到
			temp.setTaskAssignDate(historyService.createHistoricTaskInstanceQuery().taskId(task.getId()).list().get(0).getClaimTime());
			procId = task.getProcessInstanceId();
			temp.setProcid(procId);
			hpi = historyService.createHistoricProcessInstanceQuery().processInstanceId(procId).list().get(0);
			temp.setStartDate(hpi.getStartTime());//所在流程开始时间
			temp.setStartUser(hpi.getStartUserId());//所在流程发起者
			procDef = repositoryService.createProcessDefinitionQuery().processDefinitionId(task.getProcessDefinitionId()).list().get(0);
			temp.setProcname(procDef.getName());//所在流程名称
//			// 已完成任务的表单信息	主要是为了取到name
//			historyTasks = historyService.createHistoricTaskInstanceQuery().processInstanceId(procId).list();
//			tempFpsMap = new HashMap<String, String>();
//			for (HistoricTaskInstance historicTaskInstance : historyTasks) {
//				tempTaskId = historicTaskInstance.getId();
//				tempFps = formService.getTaskFormData(tempTaskId).getFormProperties();
//				for (FormProperty formProperty : tempFps) {
//					tempFpsMap.put(formProperty.getId(), formProperty.getName());
//				}
//			}
			// 已经有值的流程变量
//			vars = runtimeService.getVariables(procId);
//			for (Entry<String, Object> entry : vars.entrySet()) {
////				if(tempFpsMap.containsKey(entry.getKey()))
////					temp.addHadVars(tempFpsMap.get(entry.getKey()), entry.getValue());
//				tempVar = new VarEntity();
//				tempVar.setId(entry.getKey());
//				tempVar.setValue(entry.getValue());
//				tempVar.setType(entry.getValue().getClass().getSimpleName());
//				temp.addHadVars(tempVar);
//			}
			// formKey
			formData = formService.getTaskFormData(task.getId());
			temp.setFormKey(formData.getFormKey());
			// 完成任务需要的流程变量
//			for (FormProperty fp : formData.getFormProperties()) {
//				tempVar = new VarEntity();
//				tempVar.setId(fp.getId());
//				tempVar.setName(fp.getName());
//				tempVar.setValue(fp.getValue());
//				tempVar.setType(fp.getType().getName());
//				temp.addNeedVars(tempVar);
//			}
			result.add(temp);
		}
		return result;
	}


	/**
	 * 添加taskPage中的查询条件
	 * @param taskPage
	 * @param taskQuery
	 * @return
	 */
	private TaskQuery addWhere(TaskPage taskPage,
			TaskQuery taskQuery,String userid) {
		taskQuery = taskQuery.taskCandidateOrAssigned(userid);
		if(taskPage.getBeginTaskStartDate()!=null){
			taskQuery = taskQuery.taskCreatedAfter(taskPage.getBeginTaskStartDate());
		}
		if(taskPage.getEndTaskStartDate()!=null){
			taskQuery = taskQuery.taskCreatedBefore(taskPage.getEndTaskStartDate());
		}
		return taskQuery.orderByTaskCreateTime().asc();//.processInstanceNameLike("taskProcess");
	}

	/**
	 * 任务信息的条数
	 * @param taskPage
	 * @return
	 */
	private Long total(TaskPage taskPage,String userid) {
		TaskQuery taskQuery = taskService.createTaskQuery();
		taskQuery = addWhere(taskPage, taskQuery,userid);
		return taskQuery.count();
	}


	/**
	 * 取得一条任务信息
	 * @param taskid	
	 */
	public TaskPage get(java.lang.String taskid) {
		TaskQuery taskQuery = taskService.createTaskQuery().taskId(taskid);
		List<TaskPage> result = handle(taskQuery.list());
		return result.size()>0 ? result.get(0):null;
	}
	/**
	 * 无分页任务信息
	 */
	public List<TaskPage> listAll(TaskPage taskPage,String userid) {
		TaskQuery taskQuery = taskService.createTaskQuery();
		taskQuery = addWhere(taskPage, taskQuery,userid);
		List<Task> list = taskQuery.list();
		List<TaskPage> result = handle(list);
		return result;
	}

	/**
	 * 指定执行者	包括签收和转移(指定为自己和别人)
	 */
	@Override
	public void assign(TaskPage taskPage) {
		taskService.setAssignee(taskPage.getTaskid(), taskPage.getTaskAssignee());
		//TODO 签收时间用指定执行者时间代替	可能逻辑上不通
		taskService.claim(taskPage.getTaskid(), taskPage.getTaskAssignee());
	}

	/**
	 * 取得任务的候选人id
	 */
	public void getCandidateUsers(TaskPage taskPage) {
		List<IdentityLink> list = taskService.getIdentityLinksForTask(taskPage.getTaskid());
		List<User> users;
		for (IdentityLink identityLink : list) {
			if(identityLink.getUserId()!=null){
				users = identityService.createUserQuery().userId(identityLink.getUserId()).list();
				for (User user : users) {
					taskPage.addCandidateUsers(new ActivitiUser(user.getId(),user.getFirstName()));
				}
			}
			if(identityLink.getGroupId()!=null){
				users = identityService.createUserQuery().memberOfGroup(identityLink.getGroupId()).list();
				for (User user : users) {
					taskPage.addCandidateUsers(new ActivitiUser(user.getId(),user.getFirstName()));
				}
			}
		}
		// 没有指定任务的候选人
		if(list.size()==0){
			users = identityService.createUserQuery().list();
			for (User user : users) {
				taskPage.addCandidateUsers(new ActivitiUser(user.getId(),user.getFirstName()));
			}
		}
	}
	
	/**
	 * 根据执行者id获取执行者名字
	 */
	public String getUserNameByUserId(String userId){
		if(userId!=null)
			return identityService.createUserQuery().userId(userId).list().get(0).getFirstName();
		else
			return "";
	}

	/**
	 * 根据执行者名字获取执行者id
	 */
	private String getUserIdByUserName(String username){
		if(username!=null)
			return identityService.createUserQuery().userFirstName(username).list().get(0).getId();
		else
			return "";
	}
	
}
