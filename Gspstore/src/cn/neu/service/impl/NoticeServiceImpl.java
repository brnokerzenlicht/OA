package cn.neu.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.neu.action.NoticeServer;
import cn.neu.entity.OANoticeEntity;
import cn.neu.page.NoticePage;
import cn.neu.service.NoticeServiceI;
import cn.neu.util.MyTimer;
import cn.neu.util.MyTimerTask;

import com.jeecg.dao.BaseDaoI;
import com.jeecg.pageModel.DataGrid;
import com.jeecg.pageModel.Role;
import com.jeecg.service.RoleServiceI;
import com.jeecg.service.UserServiceI;
import com.jeecg.service.impl.BaseServiceImpl;
import com.util.dbcommon.SearchSqlGenerateUtil;

/**   
 * @Title: ServiceImpl
 * @Description: 公告通知
 *
 */
@Service("noticeService")
public class NoticeServiceImpl extends BaseServiceImpl implements NoticeServiceI {

	@Autowired
	private BaseDaoI<OANoticeEntity> noticeEntityDao;
	
	private RoleServiceI roleService;

	private UserServiceI userService;

	
	public final UserServiceI getUserService() {
		return userService;
	}
	@Autowired
	public final void setUserService(UserServiceI userService) {
		this.userService = userService;
	}
	public final RoleServiceI getRoleService() {
		return roleService;
	}
	@Autowired
	public final void setRoleService(RoleServiceI roleService) {
		this.roleService = roleService;
	}
	public BaseDaoI<OANoticeEntity> getNoticeEntityDao() {
		return noticeEntityDao;
	}
	@Autowired
	public void setNoticeEntityDao(BaseDaoI<OANoticeEntity> noticeEntityDao) {
		this.noticeEntityDao = noticeEntityDao;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	public DataGrid datagrid(NoticePage noticePage) {
		DataGrid j = new DataGrid();
		j.setRows(getPagesFromEntitys(find(noticePage)));
		j.setTotal(total(noticePage));
		return j;
	}

	private List<NoticePage> getPagesFromEntitys(List<OANoticeEntity> noticeEntitys) {
		List<NoticePage> noticePages = new ArrayList<NoticePage>();
		if (noticeEntitys != null && noticeEntitys.size() > 0) {
			for (OANoticeEntity tb : noticeEntitys) {
				// 根据entity生成page
				NoticePage b = new NoticePage(tb);
				b.setReleaseUserName(userService.findById(b.getReleaseUserId()).getRealname());
				noticePages.add(b);
			}
		}
		return noticePages;
	}

	public List<OANoticeEntity> find(NoticePage noticePage) {
		String hql = "from OANoticeEntity t where 1=1 ";

		List<Object> values = new ArrayList<Object>();
		hql = addWhere(noticePage, hql, values);

		if (noticePage.getSort() != null && noticePage.getOrder() != null) {
			hql += " order by " + noticePage.getSort() + " " + noticePage.getOrder();
		}
		return noticeEntityDao.find(hql, noticePage.getPage(), noticePage.getRows(), values);
	}

	public Long total(NoticePage noticePage) {
		return (long) listAll(noticePage).size();
	}

	private String addWhere(NoticePage noticePage, String hql, List<Object> values) {
	    //循环查询条件Page的所有[Integer][String]类型的字段，如果字段不为空则动态加上查询条件
		//-----------------------------------------------------
		StringBuffer hqlbf = new StringBuffer(hql);
		
		//根据page生成entity
		OANoticeEntity noticeEntity = new OANoticeEntity(noticePage);
		SearchSqlGenerateUtil.createSearchParamsHql(hqlbf, values, noticeEntity);
		hql = hqlbf.toString();
		//-----------------------------------------------------
		if (noticePage.getBeginTypeinTime() != null) {
			hql += " and typeinTime>=? ";
			values.add(noticePage.getBeginTypeinTime());
		}
		if (noticePage.getEndTypeinTime() != null) {
			hql += " and typeinTime<=? ";
			values.add(noticePage.getEndTypeinTime());
		}
		if (noticePage.getBeginReleaseTime() != null) {
			hql += " and releaseTime>=? ";
			values.add(noticePage.getBeginReleaseTime());
		}
		if (noticePage.getEndReleaseTime() != null) {
			hql += " and releaseTime<=? ";
			values.add(noticePage.getEndReleaseTime());
		}
		if (noticePage.getBeginExpiryTime() != null) {
			hql += " and expiryTime>=? ";
			values.add(noticePage.getBeginExpiryTime());
		}
		if (noticePage.getEndExpiryTime() != null) {
			hql += " and expiryTime<=? ";
			values.add(noticePage.getEndExpiryTime());
		}
//		// 接收人id
//		if(noticePage.getReceivedUserIds()!=null){
//			hql += " and receivedUser in (";
//			for (Integer receivedUserId : noticePage.getReceivedUserIds()) {
//				hql += receivedUserId +",";
//			}
//			hql = hql.substring(0,hql.length()-1)+")";
//		}
		return hql+" group by title";
	}

	@Transactional
	public void add(NoticePage noticePage) throws Exception {
		NoticePage temp = null;
		try {
			temp = getByTitle(noticePage.getTitle());
		} catch (Exception e) {
		}
		if(temp!=null){
			// 已经存在title 重复
			throw new Exception("标题重复，请修改后再提交！");
		}
		if(noticePage.getReceivedUserIds()!=null){
			// 设置接收人Id
			for (String receivedUserId : array_unique(noticePage.getReceivedUserIds().split(","))) {
				OANoticeEntity t = new OANoticeEntity(noticePage);
				t.setReceivedUser(receivedUserId.trim());
				noticeEntityDao.save(t);
			}
		}else{
			// 没有设置接收人Id 直接保存
			OANoticeEntity t = new OANoticeEntity(noticePage);
			noticeEntityDao.save(t);
		}
		if(noticePage.getReleaseTime().before(new Date())){
			//立即发布
			NoticeServer.sendToAll(getByTitle(noticePage.getTitle()));
		}else{
			//添加到定时器
//			BaseAction.timer.schedule(new TimerTask(get(noticePage.getTitle())),noticePage.getReleaseTime());
			MyTimer.addTask(new MyTimerTask(getByTitle(noticePage.getTitle())));
		}
	}

	// 先删除旧的在添加新的
	@Transactional
	public void update(NoticePage noticePage) {
		delete(noticePage.getTitle());
	    try {
	    	System.out.println("update:"+noticePage);
			add(noticePage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Transactional
	public void delete(String titles) {
		if (titles != null) {
			for (String title : titles.split(",")) {
				String hql = "from OANoticeEntity t where 1=1 and title = ?";
				List<OANoticeEntity> t = noticeEntityDao.find(hql,title);
			    if(t != null) {
					for (OANoticeEntity oaNoticeEntity : t) {
						noticeEntityDao.delete(oaNoticeEntity);
					}
				}
//			    BaseAction.timer.cancelTask(title);
			    MyTimer.cancelTask(title);
			}
		}
	}
	
	@Override
	public NoticePage get(NoticePage noticePage) throws Exception {
		return getByTitle(noticePage.getTitle());
	}
	
	@Override
	public NoticePage get(String title) throws Exception {
		return getByTitle(title);
	}
	
	private NoticePage getByTitle(String title) throws Exception {
		System.out.println("get:"+title);
		String hql = "from OANoticeEntity t where title = ?";
		List<OANoticeEntity> list = noticeEntityDao.find(hql,title);
		if(list==null||list.size()==0){
			// 没有找到
			throw new Exception("没有找到对应的记录！");
		}else{
			// 根据entity生成page
			NoticePage page = new NoticePage(list.get(0));
			for (OANoticeEntity oaNoticeEntity : list) {
				page.addReceivedUserIds(oaNoticeEntity.getReceivedUser());
				//page.addReceivedUserNames(userService.findById(oaNoticeEntity.getReceivedUser()).getRealname());
			}
			System.out.println("get:"+page.getReceivedUserNames()+page.getReceivedUserIds());
			return page;
		}
//		BeanUtils.copyProperties(noticeEntity,page );
	}

	public List<NoticePage> listAll(NoticePage noticePage) {
		String hql = "from OANoticeEntity t where 1=1 ";
		List<Object> values = new ArrayList<Object>();
		hql = addWhere(noticePage, hql, values);
		List<OANoticeEntity> list = noticeEntityDao.find(hql,values);
		List<NoticePage> result = getPagesFromEntitys(list);
		return result;
	}
	
	public List<NoticePage> listAllUnReleased(){
		String hql = "from OANoticeEntity t where 1=1 and releaseTime > ?";
		List<Object> values = new ArrayList<Object>();
		values.add(new Date());
		List<OANoticeEntity> list = noticeEntityDao.find(hql,values);
		Map<String,NoticePage> pageMaps = new HashMap<String,NoticePage>();
		if (list != null && list.size() > 0) {
			String title;
			for (OANoticeEntity tb : list) {
				title = tb.getTitle();
				if(pageMaps.containsKey(title)){
					pageMaps.get(title).addReceivedUserIds(tb.getReceivedUser());
					//pageMaps.get(title).addReceivedUserNames(userService.findById(tb.getReceivedUser()).getRealname());
					continue;
				}
				// 根据entity生成page
				NoticePage b = new NoticePage(tb);
				b.setReleaseUserName(userService.findById(b.getReleaseUserId()).getRealname());
				b.addReceivedUserIds(tb.getReceivedUser());
				b.addReceivedUserNames(userService.findById(tb.getReceivedUser()).getRealname());
				pageMaps.put(title, b);
			}
		}
		List<NoticePage> result = new ArrayList<NoticePage>();
		result.addAll(pageMaps.values());
		return result;
	}
	
	@Override
	public DataGrid datagridUnGroup(NoticePage noticePage,String userId) throws Exception {
		DataGrid dg = new DataGrid();
		dg.setRows(getPagesFromEntitys(findUnGroup(noticePage,userId)));
		dg.setTotal(totalUnGroup(noticePage,userId));
		return dg;
	}
	
	private Long totalUnGroup(NoticePage noticePage,String userId) throws Exception {
		List<Object> values = new ArrayList<Object>();
		String hql = geneteHql(noticePage,values,userId);
		return (long) noticeEntityDao.find(hql, values).size();
	}
	private List<OANoticeEntity> findUnGroup(NoticePage noticePage,String userId) throws Exception{
		List<Object> values = new ArrayList<Object>();
		String hql = geneteHql(noticePage,values,userId);
		List<OANoticeEntity> list = noticeEntityDao.find(hql, noticePage.getPage(), noticePage.getRows(), values);
		return list;
	}
	private String geneteHql(NoticePage noticePage,List<Object> values,String userId) throws Exception{
		String hql = "from OANoticeEntity t where 1=1";
		if(noticePage.getQueryType().equals("notice")){
			hql += " and (isAnnouncement = 1 or isAnnouncement = 2)";
		}else if(noticePage.getQueryType().equals("inform")){
			hql += " and isAnnouncement = 0";
		}else{
			throw new Exception("遇到非法标记"+noticePage.getQueryType()+"!");
		}
		if(noticePage.isQueryUnRead()){
			//是否只查看未读消息
			hql += " and isDealed = 0";
		}
		hql += " and expiryTime > ? and releaseTime < ? and (receivedUser = ? " +
				"or receivedUser = -1)";
		if (noticePage.getSort() != null && noticePage.getOrder() != null) {
			hql += " order by " + noticePage.getSort() + " " + noticePage.getOrder();
		}
		values.add(new Date());
		values.add(new Date());
		values.add(userId);
		return hql;
	}
	
	/**
	 * 用于处理阅读操作
	 */
	@Override
	@Transactional
	public NoticePage getByNoticeId(Integer noticeId) throws Exception {
		String hql = "from OANoticeEntity t where noticeId = ?";
		List<OANoticeEntity> list = noticeEntityDao.find(hql,noticeId);
		if(list==null||list.size()==0){
			// 没有找到
			throw new Exception("没有找到对应的记录！");
		}else{
			// 根据entity生成page
			OANoticeEntity temp = list.get(0);
			NoticePage page = new NoticePage(temp);
			// 修改已读标记
			temp.setIsDealed((byte)1);
			noticeEntityDao.update(temp);
			return page;
		}
	}
	
	/**
	 * 根据用户id获取该用户所有可读的未读信息
	 */
	@Override
	public List<NoticePage> getByUserId(String userId){
		String hql = "from OANoticeEntity t where isDealed = 0"
				+ " and expiryTime > ? and releaseTime < ? and (receivedUser = ? " +
				"or receivedUser = -1) order by releaseTime desc";
		List<Object> values = new ArrayList<>();
		values.add(new Date());
		values.add(new Date());
		values.add(userId);
		return getPagesFromEntitys(noticeEntityDao.find(hql, values));
	}
	
	// 去除用户角色的id和重复的用户id
	private String[] array_unique(String[] a) {
		List<String> roleIds = new ArrayList<String>();
		for(Role role:roleService.combobox()){
			roleIds.add(role.getCid());
		}
        // array_unique
        List<String> list = new ArrayList<String>();
        for(int i = 0; i < a.length; i++) {
            if(!list.contains(a[i]) && !roleIds.contains(a[i])) {
                list.add(a[i]);
            }
        }
        return (String[])list.toArray(new String[0]);
    }
}
