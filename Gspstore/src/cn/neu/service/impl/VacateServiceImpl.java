package cn.neu.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.history.HistoricVariableInstanceQuery;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.neu.page.VacatePage;
import cn.neu.service.VacateServiceI;

import com.jeecg.pageModel.DataGrid;
import com.jeecg.service.impl.BaseServiceImpl;

/**   
 * @Title: ServiceImpl
 * @Description: 请假信息
 */
@Service("vacateService")
public class VacateServiceImpl extends BaseServiceImpl implements VacateServiceI {

	private static final Logger logger = Logger.getLogger(VacateServiceImpl.class);
	@Autowired
	private HistoryService historyService;
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private TaskService taskService;
	
	/**
	 * 获取表格数据
	 */
	@Transactional(propagation = Propagation.SUPPORTS)
	public DataGrid datagrid(VacatePage vacatePage) {
		DataGrid j = new DataGrid();
		j.setRows(find(vacatePage));
		j.setTotal(total(vacatePage));
		return j;
	}

	/**
	 * 有分页的请假信息
	 * @param vacatePage
	 * @return
	 */
	private List<VacatePage> find(VacatePage vacatePage) {
		HistoricProcessInstanceQuery historyProcQuery = historyService.createHistoricProcessInstanceQuery();
		historyProcQuery = addWhere(vacatePage, historyProcQuery);
		List<HistoricProcessInstance> list = historyProcQuery.listPage((vacatePage.getPage()-1)*vacatePage.getRows(), vacatePage.getRows());
		List<VacatePage> result = handle(list);
		logger.info(result);
		return result;
	}

	/**
	 * 根据历史信息生成成VacatePage
	 * @param list
	 * @return
	 */
	private List<VacatePage> handle(List<HistoricProcessInstance> list) {
		List<VacatePage> result = new ArrayList<VacatePage>();
		HistoricVariableInstanceQuery hviq = historyService.createHistoricVariableInstanceQuery();
		List<HistoricVariableInstance> vars = new ArrayList<HistoricVariableInstance>();
		Map<String,Object> map = new HashMap<String, Object>();
		for (HistoricProcessInstance hpi : list) {
			VacatePage temp = new VacatePage();
			temp.setProcid(hpi.getId());
			vars.clear();
			vars.addAll(hviq.processInstanceId(hpi.getId()).list());
			sortVars(vars);
			varsToMap(map,vars);
			temp.setVacatename((String)map.get("vacatename"));
			temp.setStartDate((Date)map.get("startDate"));
			temp.setEndDate((Date)map.get("endDate"));
			temp.setReason((String)map.get("reason"));
			if(map.get("approve")!=null)
				temp.setApprove((Boolean)map.get("approve"));
			if(map.get("approveSuggestion")!=null)
				temp.setApproveSuggestion(((String)map.get("approveSuggestion")).trim());
			if(map.get("reApprove")!=null)
				temp.setReApprove((Boolean)map.get("reApprove"));
			if(map.get("reApproveSuggestion")!=null)
				temp.setReApproveSuggestion(((String)map.get("reApproveSuggestion")).trim());
			logger.info("历史变量");
			logger.info(map);
			// 查找运行时的变量
//			map = runtimeService.getVariables(temp.getProcid());
//			temp.setStartDate((Date)map.get("startDate"));
//			temp.setEndDate((Date)map.get("endDate"));
//			temp.setReason((String)map.get("reason"));
//			if(map.get("approve")!=null)
//				temp.setApprove((Boolean)map.get("approve"));
//			if(map.get("approveSuggestion")!=null)
//				temp.setApproveSuggestion((String)map.get("approveSuggestion"));
//			if(map.get("reApprove")!=null)
//				temp.setReApprove((Boolean)map.get("reApprove"));
//			if(map.get("reApproveSuggestion")!=null)
//				temp.setReApproveSuggestion((String)map.get("reApproveSuggestion"));
//			logger.info("运行时变量");
//			logger.info(map);
			result.add(temp);
		}
		return result;
	}

	/**
	 * 根据历史变量信息生成变量的map
	 * @param map
	 * @param vars
	 */
	private void varsToMap(Map<String,Object> map,List<HistoricVariableInstance> vars) {
		map.clear();
		for (HistoricVariableInstance var : vars) {
			map.put(var.getVariableName(), var.getValue());
		}
	}

	/**
	 * 将历史变量信息按更新时间降序排列，为了取到最新的变量信息
	 * @param vars
	 */
	private void sortVars(List<HistoricVariableInstance> vars) {
		Collections.sort(vars, new Comparator<HistoricVariableInstance>() {
			@Override
			public int compare(HistoricVariableInstance o1,
					HistoricVariableInstance o2) {
				return o1.getLastUpdatedTime().before(o2.getLastUpdatedTime())?1:-1;
			}
		});
	}

	/**
	 * 添加vacatePage中的查询条件
	 * @param vacatePage
	 * @param historyProcQuery
	 * @return
	 */
	private HistoricProcessInstanceQuery addWhere(VacatePage vacatePage,
			HistoricProcessInstanceQuery historyProcQuery) {
		historyProcQuery = historyProcQuery.processDefinitionKey("vacateProcess");
		logger.info("vacateaddwhere:"+vacatePage);
		if(vacatePage.getVacatename()!=null&&vacatePage.getVacatename().trim().length()>0){
			historyProcQuery = historyProcQuery.variableValueEquals("vacatename", vacatePage.getVacatename());
		}
		if(vacatePage.getBeginStartDate()!=null){
			historyProcQuery = historyProcQuery.variableValueGreaterThanOrEqual("startDate",vacatePage.getBeginStartDate());
		}
		if(vacatePage.getEndStartDate()!=null){
			historyProcQuery = historyProcQuery.variableValueLessThanOrEqual("startDate",vacatePage.getEndStartDate());
		}
		if(vacatePage.getBeginEndDate()!=null){
			historyProcQuery = historyProcQuery.variableValueGreaterThanOrEqual("endDate",vacatePage.getBeginEndDate());
		}
		if(vacatePage.getEndEndDate()!=null){
			historyProcQuery = historyProcQuery.variableValueLessThanOrEqual("endDate",vacatePage.getEndEndDate());
		}
		if(vacatePage.getReason()!=null&&vacatePage.getReason().trim().length()>0){
			historyProcQuery = historyProcQuery.variableValueLike("reason", vacatePage.getReason().trim());
		}
		return historyProcQuery.orderByProcessInstanceStartTime().asc();//.processInstanceNameLike("vacateProcess");
	}

	/**
	 * 请假信息的条数
	 * @param vacatePage
	 * @return
	 */
	private Long total(VacatePage vacatePage) {
		HistoricProcessInstanceQuery historyProcQuery = historyService.createHistoricProcessInstanceQuery();
		historyProcQuery = addWhere(vacatePage, historyProcQuery);
		return historyProcQuery.count();
	}

	/**
	 * 删除多条信息，以逗号分隔
	 */
	public void delete(String ids) {
		for (String id : ids.split(",")) {
			historyService.deleteHistoricProcessInstance(id);
		}
	}

	/**
	 * 取得一条请假信息
	 * @param vacateid	流程实例的ID
	 */
	public VacatePage get(VacatePage vacatePage) {
		HistoricProcessInstanceQuery historyProcQuery = historyService.createHistoricProcessInstanceQuery();
		historyProcQuery = historyProcQuery.processInstanceId(vacatePage.getProcid());
		List<VacatePage> result = handle(historyProcQuery.list());
		if(result.size()>0){
			// 查到结果	保存任务id和任务执行者  只有此种情况需要
			result.get(0).setTaskAssignee(getUserNameByUserId(vacatePage.getTaskAssignee()));
			result.get(0).setTaskid(vacatePage.getTaskid());
			return result.get(0);
		}else{
			return null;
		}
	}
	/**
	 * 无分页请假信息
	 */
	public List<VacatePage> listAll(VacatePage vacatePage) {
		HistoricProcessInstanceQuery historyProcQuery = historyService.createHistoricProcessInstanceQuery();
		historyProcQuery = addWhere(vacatePage, historyProcQuery);
		List<HistoricProcessInstance> list = historyProcQuery.list();
		List<VacatePage> result = handle(list);
		return result;
	}

	/**
	 * 开始一个请假流程并完成申请表单的填写
	 */
	@Override
	public void add(VacatePage vacatePage) {
		Map<String,Object> vars = new HashMap<String, Object>();
		vars.put("vacatename", vacatePage.getVacatename());
		vars.put("startDate", vacatePage.getStartDate());
		vars.put("endDate", vacatePage.getEndDate());
		vars.put("reason", vacatePage.getReason().trim());
		// 发起者保存的是名字  
		identityService.setAuthenticatedUserId(vacatePage.getVacatename());
		String pi = runtimeService.startProcessInstanceByKey("vacateProcess").getId();
		vacatePage.setProcid(pi);
		String taskId = taskService.createTaskQuery().processInstanceId(pi).list().get(0).getId();
		// 任务的签收使用的是id
		taskService.claim(taskId, vacatePage.getVacateid());
		taskService.complete(taskId, vars);
	}

	/**
	 * 部门负责人审批
	 */
	@Override
	public void approve(VacatePage vacatePage) {
		Map<String,Object> vars = new HashMap<String, Object>();
		vars.put("approve", vacatePage.getApprove());
		vars.put("approveSuggestion", vacatePage.getApproveSuggestion().trim());
		taskService.complete(vacatePage.getTaskid(), vars);
	}

	/**
	 * 办公室审批
	 */
	@Override
	public void reApprove(VacatePage vacatePage){
		Map<String,Object> vars = new HashMap<String, Object>();
		vars.put("reApprove", vacatePage.getReApprove());
		vars.put("reApproveSuggestion", vacatePage.getReApproveSuggestion().trim());
		taskService.complete(vacatePage.getTaskid(), vars);
	}

	/**
	 * 重新申请
	 */
	@Override
	public void restart(VacatePage vacatePage) {
		Map<String,Object> vars = new HashMap<String, Object>();
		vars.put("startDate", vacatePage.getStartDate());
		vars.put("endDate", vacatePage.getEndDate());
		vars.put("vacatename", vacatePage.getVacatename());
		vars.put("reason", vacatePage.getReason().trim());
		if(vacatePage.getApproveSuggestion()!=null)
			vars.put("approveSuggestion", vacatePage.getApproveSuggestion().trim());
		vars.put("reApprove", vacatePage.getReApprove());
		if(vacatePage.getReApproveSuggestion()!=null)
			vars.put("reApproveSuggestion", vacatePage.getReApproveSuggestion().trim());
		taskService.complete(vacatePage.getTaskid(), vars);
	}
	/**
	 * 根据执行者id获取执行者名字
	 */
	private String getUserNameByUserId(String userId){
		if(userId!=null)
			return identityService.createUserQuery().userId(userId).list().get(0).getFirstName();
		else
			return "";
	}
}
