package cn.neu.util;

//import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ScheduledExecutorService;
//import java.util.concurrent.TimeUnit;

public class MyTimer {

	private static java.util.Timer timer = new java.util.Timer();

	private static Map<String,MyTimerTask> tasks = new ConcurrentHashMap<>();

//	private static ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(3);

	public static void addTask(MyTimerTask task){
		System.err.println("before add "+tasks+":"+task);
		if(tasks.get(task.getTaskId())!=null){
			tasks.get(task.getTaskId()).cancel();
		}
		tasks.put(task.getTaskId(), task);
		timer.schedule(task, task.getNoticePage().getReleaseTime());
//		long delay = task.getNoticePage().getReleaseTime().getTime() - new Date().getTime();
		// 任务取消后仍然会执行
//		scheduledExecutor.schedule(task, delay, TimeUnit.MILLISECONDS);
		System.err.println("after add "+tasks+":"+task);
	}

	public static void cancelTask(String taskId){
		System.err.println("before cancel "+tasks);
		if(tasks.get(taskId)!=null){
			tasks.get(taskId).cancel();
			tasks.remove(taskId);
		}
		System.err.println("after cancel "+tasks);
	}


}
