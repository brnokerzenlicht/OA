package cn.neu.util;

import java.util.Date;

import cn.neu.action.NoticeServer;
import cn.neu.page.NoticePage;

/**
 * 定时任务，用于在发布时间发布任务
 */
public class MyTimerTask extends java.util.TimerTask{

	/**
	 * 发布的内容
	 */
	private NoticePage noticePage;
	//任务id
	private String taskId;
	
	public MyTimerTask(NoticePage noticePage) {
		this.noticePage = noticePage;
		this.taskId = noticePage.getTitle();
	}

	public final String getTaskId() {
		return taskId;
	}
	
	public final void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public void run() {
		System.err.println("task "+taskId+" run at "+ new Date()+" should run at :"+noticePage.getReleaseTime());
		//发布
		NoticeServer.sendToAll(noticePage);
	}

	@Override
	public String toString() {
		return "TimerTask [taskId=" + taskId + " receivedUserId = "+ noticePage.getReceivedUserIds() +"]";
	}

	public final NoticePage getNoticePage() {
		return noticePage;
	}

	public final void setNoticePage(NoticePage noticePage) {
		this.noticePage = noticePage;
	}
}
