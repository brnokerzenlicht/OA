package test.gspstore;


import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cn.neu.page.NoticePage;
import cn.neu.service.impl.NoticeServiceImpl;

public class NoticeServiceImplTest extends SpringTestBase{

	@Autowired
	private NoticeServiceImpl noticeService;
	
	@Test
	public void testDatagrid() {
		NoticePage np = new NoticePage();
		System.out.println(noticeService.datagrid(np).getRows());
	}

	@Test
	public void testListAll() throws Exception {
		NoticePage np = new NoticePage();
		np.setQueryType("inform");
		np.setQueryUnRead(false);
		System.out.println(noticeService.datagridUnGroup(np, "0").getRows());
	}

}
