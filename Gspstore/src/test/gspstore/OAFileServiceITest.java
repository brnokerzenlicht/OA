package test.gspstore;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import business.entity.supplier.SupplierEntity;
import business.service.supplier.SupplierServiceI;

import cn.neu.entity.OAFileEntity;
import cn.neu.service.OAFileServiceI;

import com.jeecg.dao.BaseDaoI;
import com.jeecg.dao.jdbc.JdbcDao;
import com.jeecg.util.SpringContextUtil;
import com.jeecg.util.excel.ExcelExportUtil;


public class OAFileServiceITest extends SpringTestBase
{
    @Autowired
    //SQL 使用JdbcDao
    private JdbcDao jdbcDao;
    @Autowired
    private OAFileServiceI oaFileService;
    @Autowired
    private BaseDaoI<OAFileEntity> oaFileEntityDao;
    @Test
    public void testAddOAFile(){
//    	System.out.println(SpringContextUtil.getBean("oaFileEntityDao"));
        OAFileEntity oaFile = new OAFileEntity();
        oaFile.setFileId(123);
        oaFile.setFilename(""+System.currentTimeMillis());
        oaFile.setFilePath("asdfa");
        System.out.println(oaFile);
        oaFileService.add(oaFile);
        System.out.println("======"+oaFileService.getOAFileById(123));
        oaFileService.delete(123);
    }
    @Test
    public void testsupplierEntityDao(){
        SessionFactory sessionFactory = SpringContextUtil.getBean("sessionFactory");  
        Session ss = sessionFactory.openSession();
        //ss.beginTransaction();
        String hql = "select new Map(LENGTH(se.stockinbillno) as max) from StockinEntity as se";
        Query q = ss.createQuery(hql);
        //System.out.println("最大ID:"+q.uniqueResult());
    }
}
