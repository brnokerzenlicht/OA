
在基于jeecg的Gspstore中添加了通知模块和流程处理模块

通知模块利用websocket实现
流程处理模块利用Activiti实现

数据库为mysql5.6.17


Gspstore登录 
	用户名admin
	密码admin
activiti-explorer登录
	用户名admin
	密码21232f297a57a5a743894a0e4a801fc3
	
activiti-explorer在官方Demo中做了修改：
	修改数据库为mysql，并且使用Gspstore中的同名视图替换的identity数据库表
	具体介绍参见：https://www.zybuluo.com/shi66yan/note/393013
	
Gspstore中使用流程处理时需要在activiti-explorer中先部署流程
